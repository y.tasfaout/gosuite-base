<?php
namespace Tests;

trait TestsSetup {
    protected function getPackageProvider($app)
    {
        return [\Gosuite\BaseServiceProvider::class];
    }

    protected function getEnvironmentSetUp($app)
    {
    // Setup default database to use sqlite :memory:
        $app['config']->set('database.default', 'mysql_testing');
        $app['config']->set('database.connections.mysql_testing', [
            'driver' => 'mysql',
            'host' => '127.0.0.1',
            'port' => '3306',
            'database' => 'gosuite_testing',
            'username' => 'root',
            'password' => '',
        ]);
    }
}