<?php

namespace Tests;
use Illuminate\Database\Eloquent\Factory;
trait MigrationSetup{
    public function migrate(){
        $this->loadLaravelMigrations(['--database' => 'mysql_testing']);
        $this->loadMigrationsFrom(realpath('database/migrations'));
        $this->artisan('migrate:refresh', [
            '--database' => 'mysql_testing'
        ]);
    }
    public function loadFactories(){
        $pathToFactories = realpath('database/factories');
        return Factory::construct(\Faker\Factory::create(), $pathToFactories);
    }
}