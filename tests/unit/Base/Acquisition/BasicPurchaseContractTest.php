<?php

namespace Tests\Unit\Base\Acquisition;

use Tests\TestsSetup;
use Tests\MigrationSetup;
use Faker\Generator as Faker;
use Orchestra\Testbench\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use \Gosuite\Base\Acquisition\BasicPurchaseContract;
use Carbon\Carbon;
use \Gosuite\Base\Resources\Physical\PhysicalResource;
use \Gosuite\Base\Resources\Human\HumanResource;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Facades\DB;
class BasicPurchaseContractTest extends TestCase
{
  use RefreshDatabase;
  use TestsSetup;
  use MigrationSetup;
  protected $basicContract;
  protected $tenant;
  protected $rentable;

  public function setUp()
  {
    parent::setUp();
    $this->migrate();
    $this->factory = $this->loadFactories();
  }

  public function tearDown()
  {
    parent::tearDown();
  }

  /**
   * It Should Create an Instance of a Purchase Contract
   * @test
   */
  public function it_should_add_an_instance_of_purchase_contract(){
    //Given
    $purchaseContract = new BasicPurchaseContract(["code" => "testcode"]);
    //Then
    $this->assertEquals("testcode",$purchaseContract->code);
  }

  /**
   * It Should Generate New Contract With Incremented Id
   * @test
   */
    public function it_should_create_contract_with_incremented_id()
    {
        //Given any contractable
        // $basicPurchaseContract = $this->factory->of(BasicPurchaseContract::class)->create();
        $pr = $this->factory->of(PhysicalResource::class)->create();
        $hr = $this->factory->of(HumanResource::class)->create();
        $date = Carbon::now();
        //When
        $contract1 = BasicPurchaseContract::createContract($pr, $hr, $date); // code : date + 00001
        $contract2 = BasicPurchaseContract::createContract($pr, $hr, $date); // code : date + 00002
        //Then
        $this->assertStringEndsWith('001', $contract1->code);
        $this->assertStringEndsWith('002', $contract2->code);
    }

    /**
     * Code Should Not Be Affected When HARD Deleting Contract
     * @test
     */
    public function code_should_not_be_affected_when_hard_deleting(){
        //Given any contractable
        // $basicPurchaseContract = $this->factory->of(BasicPurchaseContract::class)->create();
        $pr = $this->factory->of(PhysicalResource::class)->create();
        $hr = $this->factory->of(HumanResource::class)->create();
        $date = Carbon::now();
        //When
        $contract1 = BasicPurchaseContract::createContract($pr, $hr, $date); // code : date + 00001
        $contract2 = BasicPurchaseContract::createContract($pr, $hr, $date); // code : date + 00002
        $contract2->forceDelete(); // Hard Delete
        $contract3 = BasicPurchaseContract::createContract($pr, $hr, $date); // code : date + 00002
        //Then
        $this->assertStringEndsWith('001', $contract1->code);
        $this->assertStringEndsWith('002', $contract3->code);
        $this->assertDatabaseMissing('contracts', ["id" => 2]);
    }

    /**
     * It Should Reset Counting After One Year
     * @test
     */
    public function it_should_reset_code_after_a_year()
    {
        //Given any contractable
        // $basicPurchaseContract = $this->factory->of(BasicPurchaseContract::class)->create();
        $pr = $this->factory->of(PhysicalResource::class)->create();
        $hr = $this->factory->of(HumanResource::class)->create();
        $date1 = Carbon::now();
        //Given a date and name
        $date2 = new Carbon("1 January next year"); // 01/01/2020
        //When
        $contract1 = BasicPurchaseContract::createContract($pr, $hr, $date1); // code : date + 00001
        $contract2 = BasicPurchaseContract::createContract($pr, $hr, $date1); // code : date + 00002
        //When generating contract code
        $contract3 = BasicPurchaseContract::createContract($pr, $hr, $date2); // code : new date + 00001
        //Then
        $this->assertStringEndsWith('00001' , $contract3->code);
    }

    /**
     * It should not consider older codes
     * @test
     */
    public function it_should_not_consider_old_codes()
    {
        //Given any contractable
        // $basicPurchaseContract = $this->factory->of(BasicPurchaseContract::class)->create();
        $pr = $this->factory->of(PhysicalResource::class)->create();
        $hr = $this->factory->of(HumanResource::class)->create();
        $date1 = Carbon::now();
        //When
        $testCode = "0422RSB940B";
        $contract1 = new BasicPurchaseContract(["code" =>$testCode]);
        $contract1->save();
        $contract2 = BasicPurchaseContract::createContract($pr, $hr, $date1);
        $this->assertStringEndsWith('00001' , $contract2->code);
    }

  
  

}
