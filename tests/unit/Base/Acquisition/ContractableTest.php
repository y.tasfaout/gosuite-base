<?php

namespace Tests\Unit\Base\Acquisition;

use Tests\TestsSetup;
use Tests\MigrationSetup;
use Faker\Generator as Faker;
use Orchestra\Testbench\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use \Gosuite\Base\Acquisition\BasicPurchaseContract;
use Carbon\Carbon;
use \Gosuite\Base\Resources\Physical\Contracts\IPhysicalResource;
use \Gosuite\Base\Resources\Physical\PhysicalResource;
use \Gosuite\Base\Resources\Human\HumanResource;
use Illuminate\Database\Eloquent\Factory;

class ContractableTest extends TestCase
{
    use RefreshDatabase;
    use TestsSetup;
    use MigrationSetup;
    protected $basicContract;
    protected $tenant;
    protected $rentable;

    public function setUp()
    {
        parent::setUp();
        $this->migrate();
        $this->factory = $this->loadFactories();
    }

    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * Generate Contracts Directly Given
     * @param IPysicalResource or Array $pr
     * @param IHumanResource $hr
     * @param Carbon $date
     * @test
     */
    public function it_should_generate_contract_directly()
    {
        //Given any contractable
        // $basicPurchaseContract = $this->factory->of(BasicPurchaseContract::class)->create();
        $pr = $this->factory->of(PhysicalResource::class)->create();
        $hr = $this->factory->of(HumanResource::class)->create();
        $date = Carbon::now();
        //When
        $contract = BasicPurchaseContract::createContract($pr, $hr, $date);
        //Then
        $this->assertDatabaseHas(
            'sellables',
            [
                'sellable_id' => $pr->id,
                'buyer_id' => $hr->id,
                'contract_id' => $contract->id
            ]
        );
    }


    /**
     * Purchase Contract Should Add Resources With Quantity
     * @test
     */
    public function it_should_add_resources_with_quantities()
    {
        //Given
        $contract = new BasicPurchaseContract(["code" => "testcode", "start_date" => Carbon::now()]);
        $contract->save();
        $pr = $this->factory->of(PhysicalResource::class)->times(5)->create();
        //When
        $contract->addResources($pr)->withQuantities([1, 3, 6, 7, 8]);
//        eval(\Psy\sh());
        //Then
        $resources = $contract->resources()->get()->toArray();
        $this->assertCount(5, $contract->resources()->get());
        $this->assertEquals(3, $resources[1]['pivot']['qtn']);
    }

    /**
     * It Should Create Contract With Quantities
     * @test
     */
    public function it_should_create_contract_with_quantities()
    {
        //Given any contractable
        // $basicPurchaseContract = $this->factory->of(BasicPurchaseContract::class)->create();
        $pr = $this->factory->of(PhysicalResource::class)->create();
        $hr = $this->factory->of(HumanResource::class)->create();
        $date = Carbon::now();
        //When
        $contract = BasicPurchaseContract::createContract($pr, $hr, $date)->withQuantities([4]);
        //Then
        $this->assertDatabaseHas(
            'sellables',
            [
                'sellable_id' => $pr->id,
                'buyer_id' => $hr->id,
                'contract_id' => $contract->id,
                'qtn' => 4
            ]
        );
    }

    /**
     * It Should Validate Quantities
     * @test
     */
    public function it_should_validate_contract_quantities()
    {
        //Given any contractable
        // $basicPurchaseContract = $this->factory->of(BasicPurchaseContract::class)->create();
        $pr = $this->factory->of(PhysicalResource::class)->create();
        $inventory = $pr->createQuantity(7, 'Boxes');
        $hr = $this->factory->of(HumanResource::class)->create();
        $date = Carbon::now();
        //When
        $contract = BasicPurchaseContract::createContract($pr, $hr, $date)->withQuantities([4]);
        $contract->validate($inventory);
        //Then
        $this->assertEquals(3, $pr->getQuantity($inventory));
    }

    /**
     * Add Buyer to a contract
     * @test
     */
    public function it_should_add_buyer_to_contract()
    {
        //Given any contractable
        $basicPurchaseContract = $this->factory->of(BasicPurchaseContract::class)->create();
        $hr = $this->factory->of(HumanResource::class)->create();
        //When 
        $basicPurchaseContract->addBuyer($hr);
        //Then
        $this->assertCount(1, $basicPurchaseContract->getBuyer());
    }

    /**
     * Generate Contract for multiple physica resouces and single human resource
     * @test
     */
    public function it_should_generate_contract_multi_resources()
    {
        //Given
        // $basicPurchaseContract = $this->factory->of(BasicPurchaseContract::class)->create();
        $prs = $this->factory->of(PhysicalResource::class)->times(5)->create();
        $hr = $this->factory->of(HumanResource::class)->create();
        $date = Carbon::now();
        //When
        $contract = BasicPurchaseContract::createContract($prs, $hr, $date);
        //Then
        $this->assertCount(1, $contract->getBuyer());
        $this->assertCount(5, $contract->resources()->get());
    }

    /**
     * It Should Archive Contract
     * @test
     */

}