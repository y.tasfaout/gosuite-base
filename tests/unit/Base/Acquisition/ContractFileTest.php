<?php

namespace Tests\Unit\Base\Acquisition;

use Tests\TestsSetup;
use Tests\MigrationSetup;
use Faker\Generator as Faker;
use Orchestra\Testbench\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use \Gosuite\Base\Acquisition\BasicPurchaseContract;
use Carbon\Carbon;
use \Gosuite\Base\Resources\Physical\PhysicalResource;
use \Gosuite\Base\Resources\Human\HumanResource;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
class ContractFileTest extends TestCase
{
    use RefreshDatabase;
    use TestsSetup;
    use MigrationSetup;
    protected $contract;
    protected function setUp()
    {
        parent::setUp();
        $this->migrate();
        $this->factory = $this->loadFactories();
        $pr = $this->factory->of(PhysicalResource::class)->create();
        $hr = $this->factory->of(HumanResource::class)->create();
        $date = Carbon::now();
        //When
        $this->contract = BasicPurchaseContract::createContract($pr, $hr, $date);
    }
    protected function tearDown()
    {
        parent::tearDown();
    }


    /**
     * It should add files to a contract
     * @test
     */
    public function it_should_add_file_to_contract()
    {
        //Given a Contract and a file
        Storage::fake('contracts');
        //When Adding File
        $file = UploadedFile::fake()->create('bl.pdf');
        $savedFile = $this->contract->addFile($file, 0);
        //then
        //Assert if file exists
        Storage::disk('contracts')->assertExists($savedFile->url);
        //Assert Urls
        $this->assertCount(1, $this->contract->files()->get());
    }

}