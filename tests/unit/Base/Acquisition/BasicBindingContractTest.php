<?php

namespace Tests\Unit\Base\Acquisition;

use Orchestra\Testbench\TestCase;
use Tests\TestsSetup;
use Tests\MigrationSetup;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Gosuite\Base\Resources\Human\HumanResource;
use Gosuite\Base\Resources\Physical\PhysicalResource;
use Gosuite\Base\Acquisition\BasicBindingContract;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
/**
 * Binding Contracts Allow you to link multiple resources with each other
 * @package Tests\Unit\Base\Acquisition
 */
class BasicBindingContractTest extends TestCase
{
    use RefreshDatabase;
    use TestsSetup;
    use MigrationSetup;

    protected function setUp()
    {
        parent::setUp();
        $this->migrate();
        $this->factory = $this->loadFactories();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * It Should bind a resource to another
     * @test
     */
    public function it_should_add_any_resource_type_to_a_binding_contract()
    {
        //Given hr
        $hr = $this->factory->of(HumanResource::class)->create();
        $hr2 = $this->factory->of(PhysicalResource::class)->create();
        $bndContract = new BasicBindingContract(["code" => "foobar2","start_date" => new Carbon(), "end_date" => new Carbon()]);
        $bndContract->save();
        //When
        $bndContract->bind($hr2)->to($hr);
        //Then
        $this->assertCount(1, $bndContract->leftResources()->get());
        $this->assertEquals($hr->first_name, $bndContract->leftResources()->first()->toOrigin()->first_name);
        $this->assertCount(1, $bndContract->rightResources()->get());
        $this->assertEquals($hr2->name, $bndContract->rightResources()->first()->toOrigin()->name);
    }

     /**
     * It Should bind a resource to multiple resources
     * @test
     */
    public function it_should_bind_one_resource_to_multiple()
    {
        //Given hr
        $hr = $this->factory->of(HumanResource::class)->create();
        $prs = $this->factory->of(PhysicalResource::class)->times(5)->create();
        $bndContract = new BasicBindingContract(["code" => "foobar2","start_date" => new Carbon(), "end_date" => new Carbon()]);
        $bndContract->save();
        //When
        $bndContract->bind($prs)->to($hr);
        // Then
        $this->assertCount(1, $bndContract->leftResources()->get()->unique());
        $this->assertCount(5, $bndContract->rightResources()->get());
    }

    /**
     * It Should bind multiple resources to one resource
     * @test
     */
    public function it_should_bind_multiple_resources_to_one()
    {
        //Given hr
        $hr = $this->factory->of(HumanResource::class)->create();
        $prs = $this->factory->of(PhysicalResource::class)->times(5)->create();
        $bndContract = new BasicBindingContract(["code" => "foobar2","start_date" => new Carbon(), "end_date" => new Carbon()]);
        $bndContract->save();
        //When
        $bndContract->bind($hr)->to($prs);
        // Then
        $this->assertCount(5, $bndContract->leftResources()->get()->unique());
        $this->assertCount(1, $bndContract->rightResources()->get()->unique());
    }

     /**
     * It Should bind multiple resources to multiple resource
     * @test
     */
    public function it_should_bind_multiple_resources_to_multiple()
    {
        //Given hr
        $hrs = $this->factory->of(HumanResource::class)->times(4)->create();
        $prs = $this->factory->of(PhysicalResource::class)->times(2)->create();
        eval(\Psy\sh());
        $bndContract = new BasicBindingContract(["code" => "foobar2","start_date" => new Carbon(), "end_date" => new Carbon()]);
        $bndContract->save();
        //When
        $bndContract->bind($hrs)->to($prs);
        // Then
        $this->assertCount(2, $bndContract->leftResources()->get()->unique());
        $this->assertCount(4, $bndContract->rightResources()->get()->unique());
    }
    /**
     * It Should Bind One Human Resource to Another
     * @test
     */
    public function it_should_bind_one_human_resource_to_another()
    {
        //Given Two HRs
        $hr1 = $this->factory->of(HumanResource::class)->create();
        $hr2 = $this->factory->of(HumanResource::class)->create();
        //When
        $bindingContract = BasicBindingContract::bindResources($hr1,$hr2);
        //Then
        $this->assertCount(1,$bindingContract->getLeftResources());
    }


    /**
     * It Should Update Resources of a contract
     * @test
     */
    public function it_should_update_resources_of_contract()
    {
        //Given Two HRs
        $hr1 = $this->factory->of(HumanResource::class)->create();
        $hr2 = $this->factory->of(HumanResource::class)->create();
        //When
        $bindingContract = BasicBindingContract::bindResources($hr1,$hr2);
        $hr3 = $this->factory->of(HumanResource::class)->create(['first_name' => 'new client']);
        $hr4 = $this->factory->of(HumanResource::class)->create(['first_name' => 'new client2']);
        $bindingContract->updateResources('right', collect([$hr3,$hr4]));
        eval(\Psy\sh());
        //Then
        $this->assertEquals($hr3->first_name,$bindingContract->getRightResources()->first()->toOrigin()->first_name);
        $this->assertCount(2, $bindingContract->getRightResources());
        $this->assertCount(1, $bindingContract->getLeftResources());
        $this->assertEquals($hr1->first_name, $bindingContract->getLeftResources()->first()->toOrigin()->first_name);
    }

    
}
