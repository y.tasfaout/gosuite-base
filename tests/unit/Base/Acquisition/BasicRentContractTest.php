<?php

namespace Tests\Unit\Base\Acquisition;

use Tests\TestsSetup;
use Tests\MigrationSetup;
use Faker\Generator as Faker;
use Orchestra\Testbench\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use \Gosuite\Base\Acquisition\BasicRentContract;
use Carbon\Carbon;
use \Gosuite\Base\Resources\Contracts\IPhysicalResource;
use \Gosuite\Base\Resources\Physical\PhysicalResource;
use \Gosuite\Base\Resources\Human\HumanResource;
use Illuminate\Database\Eloquent\Factory;

class BasicRentContractTest extends TestCase
{
  use RefreshDatabase;
  use TestsSetup;
  use MigrationSetup;
  protected $basicContract;
  protected $tenant;
  protected $rentable;

  public function setUp()
  {
    parent::setUp();
    $this->migrate();
    $this->factory = $this->loadFactories();
    $startDate = new Carbon("00:00");
    $endDate = new Carbon("+2 days");
    $endDate->setTimeFromTimeString("12:00");
    $this->tenant = $this->factory->of(HumanResource::class)->create();
    $this->rentable = $this->factory->of(PhysicalResource::class)->create();
    $contractCode = rentContractCode($startDate, $this->tenant->first_name);
    $this->basicContract = $this->factory->of(BasicRentContract::class)->create(["code" => $contractCode, "start_date" => $startDate, "end_date" => $endDate]);
    $this->basicContract->addTenant($this->tenant, $this->rentable);
    $this->basicContract->addResources($this->rentable);
  }

  public function tearDown()
  {
    parent::tearDown();
    unset($this->tenant);
    unset($this->rentable);
    unset($this->basicContract);
  }

  /**
   * It should create a BasicRentContract Instance
   * @test
   */
  public function it_should_create_an_instance_rent_contract()
  {
    	//Create an instacnce
    $basicContract = new BasicRentContract(["code" => "testcode", "start_date" => Carbon::now(), "end_date" => Carbon::now()]);
    	//Simple value check
    $this->assertEquals("testcode", $basicContract->code, 'This should equal');
  }

  /**
   * It should create a DB instance of BasicRentContract
   * Issued Contract
   * @test
   */
  public function it_should_create_db_instance_rent_contract()
  {
    	//Db instance of a basic contract
    $basicContract = $this->factory->of(BasicRentContract::class)->create();
    	//Simple count check should return two, since we already initiated a contract in setUp
    $this->assertCount(2, BasicRentContract::all());
  }

  /**
   * It should cancel a contract
   * @test
   */
  public function it_should_cancel_rent_contract()
  {
    	//Given a contract
    $basicContract = $this->factory->of(BasicRentContract::class)->create();
    	//When cancelling contract
    $basicContract->cancel();
    	//Field cancel should be true
    $this->assertTrue($basicContract->isCancelled());
  }

  /**
   * It Should link physical resources to a contract
   * @test
   */
  public function it_should_add_a_resource()
  {
    	//Given a empty contract
    $basicContract = $this->factory->of(BasicRentContract::class)->create();
    	//Given a physical resource
    $resource = $this->factory->of(PhysicalResource::class)->create();
    	//When Adding Resources
    $basicContract->addResources($resource);
    	//Then
    $this->assertCount(1, $basicContract->resources()->get());
  }

  /**
   * It should add multiple resources
   * @test
   */
  public function it_should_add_multiple_resources()
  {
    	//Given an empty contract
    $basicContract = $this->factory->of(BasicRentContract::class)->create();
    	//Given multiple physical resources
    $resources = $this->factory->of(PhysicalResource::class)->times(5)->create();
    	//When adding resources
    $basicContract->addResources($resources);
    	//Then
    $this->assertCount(5, $basicContract->resources()->get());
  }

  /**
   * It Should create a contract for a resources its children
   * @test
   */
  public function it_should_issue_a_contract_for_resource_its_children()
  {
    	//Given an empty contract
    $basicContract = $this->factory->of(BasicRentContract::class)->create();
    	//Given a root physical node
    $physicalResourceRoot = $this->factory->of(PhysicalResource::class)->create(["structural" => true]);
    	//Add as children
    $physicalResourceRoot->children()->create(["name" => "test", "description" => "test"]);
    $physicalResourceRoot->children()->create(["name" => "test2", "description" => "test2"]);
    $child = $physicalResourceRoot->children()->create(
      ["name" => "test3", "description" => "test3", "structural" => true]
    );
    $child->children()->create(["name" => "test4", "description" => "test4"]);
    	//When adding resources
    $basicContract->addResources($physicalResourceRoot);
    	//Then
    $this->assertCount(3, $basicContract->resources()->get());
  }

  /**
   * It Should create contract only for non structural resources
   * @test
   */
  public function it_should_rent_only_non_structural_resource()
  {
    	//Given an empty contract
    $basicContract = $this->factory->of(BasicRentContract::class)->create();
    	//Given a structural physical node
    $structuralNode = $this->factory->of(PhysicalResource::class)->create(['structural' => true]);
    	//When adding resource
    $basicContract->addResources($structuralNode);
    	//Then
    $this->assertCount(0, $basicContract->resources()->get());
  }


  /**
   * It should add a tenant to a contract
   * @test
   */
  public function it_should_add_tenant_to_contract()
  {
		//Given an empty contract
    $basicContract = $this->factory->of(BasicRentContract::class)->create();
    	//Given Physical Resource
    $physicalResource = $this->factory->of(PhysicalResource::class)->create();
		//Add Physical Resource
    $basicContract->addResources($physicalResource);
		//Given a tenant
    $tenant = $this->factory->of(HumanResource::class)->create();
		//When Adding a tenant must have a physicalResource
    $basicContract->addTenant($tenant, $physicalResource);
		//Then
    $this->assertCount(1, $basicContract->getTenants());

  }
  /**
   * It Should Check if a contract is expired
   * @test
   */
  public function it_should_check_contract_expiry()
  {
		//Given a contract startDate: today, endDate : +2 days
    $contract = $this->basicContract;
		//When
    $isExpired = $contract->isExpired();
		//then should not be expired
    $this->assertFalse($isExpired);
		//Given an other contract startDate: 2 days before , endDate : today
    $otherContract = $this->factory->of(BasicRentContract::class)->create(["start_date" => new Carbon("-2 days"), "end_date" => new Carbon()]);
		//then should be expired
    $this->assertTrue($otherContract->isExpired());
  }

  /**
   * Check Expiry given a expiry time
   * @test
   */
  public function it_should_check_expiry_given_time()
  {
		//Given Contract that expires today
    $contract = $this->basicContract;
    $contract->end_date->subDays(2);
		//When
    $isExpired = $contract->isExpired("00:00");
		//Then
    $this->assertTrue($isExpired);
  }


  /**
   * It Should Check if a contract has started
   * @test
   */
  public function it_should_check_contract_has_started()
  {
		// Given a contract startDate: today 00:00, endDate : +2 days 12:00
    $contract = $this->basicContract;
		// When
    $hasStarted = $contract->hasStarted();
		// Then
    $this->assertTrue($hasStarted);
    $time = Carbon::now("+2 hours")->hour . ":00";
		// When we specify a starting time
    $hasStarted = $contract->hasStarted($time);
		//Then
    $this->assertFalse($hasStarted);
  }

  /**
   * It Should Extend a Contract
   * This will extend all rentable end date
   * @test
   */
  public function it_should_extend_contract()
  {
		//Given a contract endDate is +2 days from now
    $contract = $this->basicContract;
		// New Ending Date
    $newEndDate = new Carbon("+4 days");
		//When
    $contract->extend($newEndDate);
		//Then
    $this->assertEquals($newEndDate, $contract->end_date);
  }

  /**
   * It Should not extend if resources are not available
   * @test
   */
  public function it_should_not_extend_non_available_resources()
  {
    //Given a contract endDate is +2 days from now
    $contract = $this->basicContract;
    $contract->addResources($this->rentable);
    //Rent it's resource after the previous contract finishes
    $newTenant = $this->factory->of(HumanResource::class)->create();
    $newContract = $this->rentable->rent($newTenant, new Carbon("+3 days"), new Carbon("+4 days"));
    //Assert that rentable has 2 contracts now
    $this->assertCount(2, $this->rentable->contracts()->get());
    //When extending previous contract should throw exception
    $this->expectException('Exception');
    $extendDate = new Carbon("+4 days");
    $contract->extend($extendDate);
  }

  /**
   * It should check if all resources are not available
   * @test
   */
  public function it_should_check_availability_of_all_resources()
  {
    // Given a contract
    $contract = $this->basicContract;
    // Resources
    $resources = $this->factory->of(PhysicalResource::class)->times(4)->create();
    // Adding resources
    $contract->addResources($resources);
    // When
    $startDate = new Carbon("+1 days");
    $endDate = new Carbon("+2 days");
    $resourcesAreAvailable = $contract->attachedResources()
      ->areAvailable($startDate, $endDate);
    // Then
    $this->assertFalse($resourcesAreAvailable);
  }
  
}
