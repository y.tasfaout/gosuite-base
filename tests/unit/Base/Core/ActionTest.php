<?php

namespace Tests\Unit\Base\Core;

use Orchestra\Testbench\TestCase;
use Tests\TestsSetup;
use Tests\MigrationSetup;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Gosuite\Base\Core\Action;
use Gosuite\Base\Resources\Human\HumanResource;
use Illuminate\Support\Facades\DB;
class ActionTest extends TestCase
{
    use RefreshDatabase;
    use TestsSetup;
    use MigrationSetup;

    /**
     * Setup 
     */
    public function setUp(){
        parent::setUp();
        $this->migrate();
        $this->factory = $this->loadFactories();
    }
    /**
     * Tear Down
     */
    public function tearDown(){
        parent::tearDown();
    }

    /**
     * Should Create Action
     * @test
     */
    public function it_should_create_action()
    {
        //Given
        $action = new Action(["resource_type" => HumanResource::class, "action_type" => "c"]);
        $action->save();
        //Then
        $this->assertEquals(HumanResource::class, $action->resource_type);
    }
    
    /**
     * Should List Resources Actions
     * @test
     */
    public function it_should_list_actions_supported_by_resource()
    {
        //Given
        $actionsArray = array('c','r','u','d');
        foreach ($actionsArray as $actionType) {
            $action = new Action(["resource_type" => HumanResource::class, "action_type" => $actionType]);
            $action->save();
        }
        //When
        $actionsByResource = Action::byResource(HumanResource::class)->get();
        //Then
        $this->assertEquals(HumanResource::class, $actionsByResource->toArray()[0]["resource_type"]);
        $this->assertCount(4, $actionsByResource);
    }

}

