<?php

namespace Tests\Unit\Base\Resources;

use Gosuite\Base\Acquisition\BasicBindingContract;
use Orchestra\Testbench\TestCase;
use Tests\TestsSetup;
use Tests\MigrationSetup;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Gosuite\Base\Resources\Human\HumanResource;
use Gosuite\Base\Resources\Physical\PhysicalResource;
use Gosuite\Base\Resources\Resource;

/**
 * The Resource Super Class is Used to Help Create Binding Contracts
 * Where it Acts Like a Parent to Other Resources Type While Staying Generic
 */
class ResourceTest extends TestCase
{
    use RefreshDatabase;
    use TestsSetup;
    use MigrationSetup;

    protected function setUp()
    {
        parent::setUp();
        $this->migrate();
        $this->factory = $this->loadFactories();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * It should have one related human resource
     * @test
     */
    public function it_should_get_related_human_resource()
    {
        //Given
        $hr = $this->factory->of(HumanResource::class)->create();
        //When
        $resource = new Resource(['resource_id' => $hr->id, 'resource_type' => get_class($hr)]);
        $resource->save();
        //Then
        $this->assertEquals($hr->id, $resource->human()->first()->id);
        $this->assertEquals(get_class($hr), $resource->human()->first()->type);
    }

    /**
     * It should have one related physical resource
     * @test
     */
    public function it_should_get_related_physical_resource()
    {
        //Given
        $pr = $this->factory->of(PhysicalResource::class)->create();
        //When
        $resource = new Resource(['resource_id' => $pr->id, 'resource_type' => get_class($pr)]);
        $resource->save();
        //then
        $this->assertEquals($pr->id, $resource->physical()->first()->id);
        $this->assertEquals(get_class($pr), $resource->physical()->first()->type);
    }


    /**
     * It should not conflict
     * @test
     */
    public function it_should_not_conflict_related_models()
    {
        //Given
         $pr = $this->factory->of(PhysicalResource::class)->create();
         $hr = $this->factory->of(HumanResource::class)->create();
         $hr2 = $this->factory->of(HumanResource::class)->create();
        //When
         $resource1 = Resource::register($hr);
         $resource2 = Resource::register($pr);
        //Then
        $this->assertEquals($hr->first_name, $resource1->human()->first()->first_name);
        $this->assertEquals($pr->name, $resource2->physical()->first()->name);
    }

    /**
     * Get Original Resource
     * @test
     */
    public function it_should_get_original_resource()
    {
        //Given 
        $hr = $this->factory->of(HumanResource::class)->create();
        $pr = $this->factory->of(PhysicalResource::class)->create();
        //When
        $genericResource1 = Resource::register($hr);
        $genericResource2 = Resource::register($pr);
        $original1 = $genericResource1->toOrigin();
        $original2 = $genericResource2->toOrigin();
        //Then
        $this->assertEquals($hr->type, $original1->type);
        $this->assertEquals($pr->type, $original2->type);
    }


    /**
     * It should find generic resource given explicit resource
     * @test
     */
    public function it_should_find_generic_resource_from_explicit()
    {
        //Given explicit resource
        $hr = $this->factory->of(HumanResource::class)->create();
        $hr2 = $this->factory->of(HumanResource::class)->create();
        $bndContract = BasicBindingContract::bindResources($hr, $hr2);
        //When
        $retrievedResource = Resource::where('resource_id', $hr->id)->first();
        $contract = $retrievedResource->contracts('left')->first();
        //Then
        $this->assertEquals($bndContract->code, $contract->code);
    }

    /**
     * It should delete resource contracts
     * @test
     */
    public function it_should_delete_contracts_from_explicit()
    {
        //Given explicit resource
        $hr = $this->factory->of(HumanResource::class)->create();
        $hr2 = $this->factory->of(HumanResource::class)->create();
        $bndContract = BasicBindingContract::bindResources($hr, $hr2);
        //When
        $retrievedResource = Resource::where('resource_id', $hr->id)->first();
//        eval(\Psy\sh());
        $contract = $retrievedResource->contracts('left')->detach();
        $bndContract->delete();
        //Then
        $this->assertNotNull($bndContract->deleted_at);
    }

}
