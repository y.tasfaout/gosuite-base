<?php

namespace Tests\Unit\Base\Resources\Physical;

use Orchestra\Testbench\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Gosuite\Base\Resources\Contracts\IPhysicalResource;
use Gosuite\Base\Resources\Physical\PhysicalResource;
use Gosuite\Base\Resources\Human\HumanResource;
use Gosuite\Base\Acquisition\BasicRentContract;
use Gosuite\Base\Acquisition\BasicPurchaseStrategy;
use Carbon\Carbon;
use Tests\TestsSetup;
use Tests\MigrationSetup;

class PhysicalResourceTest extends TestCase
{
  use RefreshDatabase;
  use TestsSetup;
  use MigrationSetup;
  protected $physicalResource;
  protected $tenant;
  public function setUp()
  {
    parent::setUp();
    $this->migrate();
    $this->factory = $this->loadFactories();
    $this->physicalResource = $this->factory->of(PhysicalResource::class)->create();
    $this->tenant = $this->factory->of(HumanResource::class)->create();
  }

  public function tearDown()
  {
    parent::tearDown();
    unset($this->physicalResource);
    unset($this->tenant);
  }
  /**
   * Should create an instance of physical resource
   * @test
   */
  public function it_should_create_an_instance_of_physical_resource()
  {
 		//Given Physical Resource
    $physicalResource = new PhysicalResource(["name" => "Hotel", "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam itaque consequatur modi veritatis omnis veniam sint odio, quae voluptatem excepturi, consectetur sequi a, quas earum magnam quisquam asperiores deserunt doloremque."]);
    $this->assertEquals("Hotel", $physicalResource->name, 'It should be equal to Hotel');
  }

  /**
   * Should be a root when created
   * @test
   */
  public function it_should_be_root_node()
  {
 		//Given
    $physicalRootNode = $this->factory->of(PhysicalResource::class)->create();
 		//Then
    $this->assertTrue($physicalRootNode->isRoot());
  }
  /**
   * Should add children
   * @test
   */
  public function it_should_add_children()
  {
 		//Given a root node
    $physicalRootNode = $this->factory->of(PhysicalResource::class)->create();
 		//When Adding Children
    $physicalRootNode->children()->create(["name" => "Building 1", "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis et mollitia sapiente molestiae nam sint natus beatae quod molestias veritatis voluptatem velit hic, quam iusto, temporibus harum maxime ut dolore."]);
 		//Then Assert Count
    $this->assertCount(1, $physicalRootNode->children()->get());
  }

  /**
   * It should be rentable
   * @test
   */
  public function it_should_be_rentable()
  {
		//Given
		//Tenant
    $tenant = $this->getMockBuilder('Gosuite\Base\Resources\Human\HumanResource')
      ->setMethods(['contracts'])
      ->getMock();
		//Start Date and End Date
    $startDate = new Carbon('08/05/2018 00:00');
    $endDate = new Carbon('12/05/2018 12:00');
		//Contract Mock To Assert With
    $contract = $this->getMockBuilder('Gosuite\Base\Acquisition\BasicRentContract')
      ->setMethods(['addResources'])
      ->getMock();
		//Resource
    $physicalResource = $this->getMockBuilder('Gosuite\Base\Resources\Physical\PhysicalResource')
      ->setMethods(['rent'])
      ->getMock();
		//When Renting Should Return a Contract Type
    $physicalResource->method('rent')
      ->with($tenant)
      ->will($this->returnValue($contract));
		//Then Assert Instance
    $this->assertInstanceOf(BasicRentContract::class, $physicalResource->rent($tenant, $startDate, $endDate));
  }

  /**
   * It should add appropriate entries in DB
   * @test
   */
  public function it_should_add_db_entries_when_renting()
  {
		//Given
		// Start Date & End Date
    $startDate = new Carbon('08/05/2018 00:00');
    $endDate = new Carbon('12/05/2018 12:00');
		//When renting we need a tenant and dates
    $contract = $this->physicalResource->rent($this->tenant, $startDate, $endDate);
		//Then
    $this->assertDatabaseHas('contracts', [
      'start_date' => $contract->start_date,
      'end_date' => $contract->end_date
    ]);
    $this->assertDatabaseHas('rentables', [
      'rentable_id' => $this->physicalResource->id,
      'rentable_type' => get_class($this->physicalResource),
      'tenant_id' => $this->tenant->id
    ]);
  }
  /**
   * It should add a client to a physical resource
   * @test
   */
  public function it_should_add_a_tenant_to_resource()
  {
		//Given
		// A Physical Resource That has contract
		// Start Date & End Date
    $startDate = new Carbon('08/05/2018 00:00');
    $endDate = new Carbon('12/05/2018 12:00');
		//When
    $contract = $this->physicalResource->rent($this->tenant, $startDate, $endDate);
		// Adding an other tenant
    $otherTenant = $this->factory->of(HumanResource::class)->create();
    $this->physicalResource->addTenant($otherTenant);
		//Then
    $this->assertCount(2, $this->physicalResource->getTenants());
  }

  /**
   * It should check availablity of a free physical resources
   * @test
   */
  public function it_should_check_availability_of_a_free_resource()
  {
		//Given
		//Start Date and End Date
    $startDate = new Carbon('08/05/2018 00:00');
    $endDate = new Carbon('12/05/2018 12:00');
		//When
    $isAvailable = $this->physicalResource->isAvailable($startDate, $endDate);
		//Then
    $this->assertTrue($isAvailable);
  }

  /**
   * It should check availablity of a rented physical resource
   * @dataProvider provideDates
   * @test
   */
  public function it_should_check_availability_of_a_taken_resource($startDate, $endDate, $expectedResult)
  {
		//Given
		//Start Date and End Date
    $contractStartDate = new Carbon('05/08/2018 00:00');
    $contractEndDate = new Carbon('05/12/2018 12:00');
		//When
    $contract = $this->physicalResource->rent($this->tenant, $contractStartDate, $contractEndDate);
		//When
    $isAvailable = $this->physicalResource->isAvailable($startDate, $endDate);
		//Then
    $this->assertEquals($isAvailable, $expectedResult);
  }

  /**
   * Dates Povider
   */
  public function provideDates()
  {
    return [
      "Same Contract Dates" => [new Carbon('05/08/2018 00:00'), new Carbon('05/12/2018 12:00'), false],
      "One Day Plus Contract StartDate Same EndDate" => [new Carbon('05/09/2018 00:00'), new Carbon('05/12/2018 12:00'), false],
      "After Contract Ends" => [new Carbon('05/12/2018 13:00'), new Carbon('05/16/2018 12:00'), true],
      "Before Contract But After Contract StartDate" => [new Carbon('05/05/2018 00:00'), new Carbon('05/10/2018 12:00'), false],
      "Before Contract But After Contract EndDate" => [new Carbon('05/05/2018 00:00'), new Carbon('05/14/2018 12:00'), false],
      "Before Contract Before Contract StartDate" => [new Carbon('05/05/2018 00:00'), new Carbon('05/06/2018 12:00'), true]
    ];
  }

  /**
   * It should free given a contract code
   * @test
   */
  public function it_should_be_free_from_a_given_contract()
  {
		//Given a rentable
    $rentable = $this->physicalResource;
    $tenant = $this->tenant;
		//Dates
    $startDate = new Carbon();
    $endDate = new Carbon("+2 days");
		//Contract
    $contract = $rentable->rent($tenant, $startDate, $endDate);
		//When Freeing Up The Resource
    $rentable->cancelContract($contract);
		//Then
    $this->assertCount(0, $rentable->contracts()->get());
  }

  /**
   * It Should Free on all contracts
   * @test
   */
  public function it_should_free_all_contracts()
  {
		//Given
    $rentable = $this->physicalResource;
    $tenant = $this->tenant;
		//Dates
    $startDate = new Carbon();
    $endDate = new Carbon("+2 days");
		//Contact 1
    $contract1 = $rentable->rent($tenant, $startDate, $endDate);
		//Contract 2
    $contract2 = $rentable->rent($tenant, $startDate->addDays(3), $endDate->addDays(2));
		//Then
    $this->assertCount(2, $rentable->contracts()->get());
		//When Delete
    $rentable->cancelAllContracts();
		//Then
    $this->assertCount(0, $rentable->contracts()->get());
  }

  /**
   * It should throw an exception when renting on non available resource
   * @test
   */
  public function it_should_throw_exception_when_rent_on_non_available_resource()
  {
		//Given
    $rentable = $this->physicalResource;
    $tenant = $this->tenant;
		//Dates
    $startDate = new Carbon();
    $endDate = new Carbon("+2 days");
		//Contact
    $contract = $rentable->rent($tenant, $startDate, $endDate);
		//When Renting again should throw non-available exception
    $this->expectException('Exception');
    $newContract = $rentable->rent($tenant, new Carbon("+1 day"), new Carbon("+3 days")); // rentable should be unavailable
    $this->assertFalse($rentable->isAvailable(new Carbon("+1 day"), new Carbon("+3 days")));
    $this->assertCount(1, $rentable->contracts()->get());
  }

  /**
   * It should delete resource
   * @test
   */
  public function it_should_delete_physical_resource()
  {
    //Given
    $resource = $this->physicalResource;
    //When
    $resource->delete();
    //Then
    $this->assertNotNull($resource->deleted_at);
  }

  /**
   * It should throw exception when deleting a resource that has contracts
   * @test
   */
  public function it_should_not_delete_rented_resource()
  {
    //Given
    $rentable = $this->physicalResource;
    $tenant = $this->tenant;
		//Dates
    $startDate = new Carbon();
    $endDate = new Carbon("+2 days");
		//Contact
    $contract = $rentable->rent($tenant, $startDate, $endDate);
    //Assert Exception
    $this->expectException("Exception");
    //When
    $rentable->remove();
    //Then
    $this->assertNull($rentable->deleted_at);
  }
  /**
   * It should throw exception when deleting a resource that has children
   * @test
   */
  public function it_should_not_delete_parent_resources()
  {
    //Given a root node
    $physicalRootNode = $this->factory->of(PhysicalResource::class)->create();
 		//Adding Children
    $physicalRootNode->children()->create(["name" => "Building 1", "description" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis et mollitia sapiente molestiae nam sint natus beatae quod molestias veritatis voluptatem velit hic, quam iusto, temporibus harum maxime ut dolore."]);
    //When Deleting
    //Assert Exception
    $this->expectException("Exception");
    //When
    $physicalRootNode->remove();
    //Then
    $this->assertNull($physicalRootNode->deleted_at);
  }


  /**
   * It should buy sellable 
   * @test
   */
  public function it_should_be_sellable()
  {
    //Given a physical resource & human resource
    $product = $this->factory->of(PhysicalResource::class)->create();
    $client = $this->factory->of(HumanResource::class)->create();
    $purchaseDate = Carbon::now();
    $purchaseStrategy = new BasicPurchaseStrategy();
    //When Buying
    $purchaseContract = $product->sell($purchaseStrategy, $client, $purchaseDate);
    //Then
    $this->assertDatabaseHas('contracts', [
      'code' => $purchaseContract->code,
      'start_date' => $purchaseDate
    ]);
  }

  /**
   * It should be able to set quantity
   * @test
   */
  public function it_should_set_quanity()
  {
    //Given
    $pr = $this->factory->of(PhysicalResource::class)->create();
    $qtn = 2;
    $unit = "Boxes";
    //When
    $pr->createQuantity($qtn,$unit);
    //Then
    $this->assertEquals($qtn, $pr->getQuantity());
    $this->assertEquals($unit, $pr->getUnit());
  }

    /**
     * It should update quantity
     * @test
     */
    public function it_should_update_quantity()
    {
        //Given
        $pr = $this->factory->of(PhysicalResource::class)->create();
        $qtn = 2;
        $newQtn = 1;
        $unit = "Boxes";
        //When
        $inventory = $pr->createQuantity($qtn, $unit);
        // eval(\Psy\sh());
        $pr->updateQuantity($inventory, $newQtn);
        //Then
        $this->assertEquals(1, $pr->getQuantity());
    }

  /**
   * It should consume quantity
   * @test
   */
    public function it_should_consume_quantity()
    {
        //Given
        $pr = $this->factory->of(PhysicalResource::class)->create();
        $qtn = 2;
        $consumedQtn = 1;
        $unit = "Boxes";
        //When
        $inventory = $pr->createQuantity($qtn, $unit);
        // eval(\Psy\sh());
        $pr->consumeQuantity($inventory, $consumedQtn);
        //Then
        $this->assertEquals(1, $pr->getQuantity());
    }
}
