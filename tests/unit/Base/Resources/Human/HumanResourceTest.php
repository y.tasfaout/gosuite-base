<?php

namespace Tests\Unit\Base\Resources\Human;

use Carbon\Carbon;
use Gosuite\Base\Acquisition\BasicPurchaseContract;
use Orchestra\Testbench\TestCase;
use Gosuite\Base\Acquisition\BasicRentContract;
use Gosuite\Base\Resources\Human\HumanResource;
use Illuminate\Foundation\Testing\WithFaker;
use Gosuite\Base\Resources\Physical\PhysicalResource;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestsSetup;
use Tests\MigrationSetup;

class HumanResourceTest extends TestCase
{
  use RefreshDatabase;
  use TestsSetup;
  use MigrationSetup;

  /**
   * Setup 
   */
  public function setUp(){
    parent::setUp();
    $this->migrate();
    $this->factory = $this->loadFactories();
  }
  /**
   * Tear Down
   */
  public function tearDown(){
    parent::tearDown();
  }
  /**
   * It should create an instance of HumanResource
   * @test
   */
  public function it_should_create_an_instance_human_resource()
  {
    $human = new HumanResource(["first_name" => "Yassine"]);
    $this->assertEquals("Yassine", $human->first_name);
  }

  /**
   * It should create a single entry in DB
   * @test
   */
  public function it_should_create_human_entry_in_db()
  {
    $human = $this->factory->of(HumanResource::class)->create();
    $this->assertCount(1, HumanResource::all());
  }


  /**
   * It should update a human resource
   * @test
   */
  public function it_should_update_human_resource()
  {
    //Given
    $human = $this->factory->of(HumanResource::class)->create();
    //When
    $human->update(["title", "first_name" => "Tasfaout", "last_name" => "Yassine"]);
    //Then
    $this->assertEquals("Tasfaout", $human->first_name);
  }

  /**
   * It should delete a human resource
   * @test
   */
  public function it_should_delete_human_resource()
  {
    //Given
    $human = $this->factory->of(HumanResource::class)->create();
    //When
    $human->remove();
    //Then
    $this->assertNotNull($human->deleted_at);
  }

  /**
   * It should get contracts
   * @test
   */
  public function it_should_get_contracts()
  {
    //Given an empty contract
    $basicContract = $this->factory->of(BasicRentContract::class)->create();
    //Given Physical Resource
    $physicalResource = $this->factory->of(PhysicalResource::class)->create();
		//Add Physical Resource
    $basicContract->addResources($physicalResource);
		//Given a tenant
    $tenant = $this->factory->of(HumanResource::class)->create();
		//Adding a tenant must have a physicalResource
    $basicContract->addTenant($tenant, $physicalResource);
    //Then
    $this->assertCount(1, $tenant->contracts()->get());
  }
  /**
   * It should throw exception if the human resource has a active contract
   * @test
   */
  public function it_should_throw_exception_on_delete_tenant()
  {
    //Given an empty contract
    $basicContract = $this->factory->of(BasicRentContract::class)->create();
    //Given Physical Resource
    $physicalResource = $this->factory->of(PhysicalResource::class)->create();
		//Add Physical Resource
    $basicContract->addResources($physicalResource);
		//Given a tenant
    $tenant = $this->factory->of(HumanResource::class)->create();
		//When Adding a tenant must have a physicalResource
    $basicContract->addTenant($tenant, $physicalResource);
    //Assert Exception
    $this->expectException("Exception");
    //When Deleting Tenant
    $tenant->remove();
    //Then
    $this->assertNull($tenant->deleted_at);
  }

  /**
   * It Should Get Contracts Given Type
   * @test
   */
    public function it_should_get_contracts_given_type()
    {
        //Given
            //Human Resource
        $hr = $this->factory->of(HumanResource::class)->create();
            //Physical Resource
        $pr = $this->factory->of(PhysicalResource::class)->create();
            //Contract
        $cr = BasicPurchaseContract::createContract($pr,$hr,Carbon::now());
        //When
            //Get Contracts
        $contracts = $hr->contracts($cr)->get()->toArray();
        //Then
            //Assert Type Of Returned Contracts
            $this->assertEquals(get_class($cr), $contracts[0]["type"]);
    }

    /**
     * It Should Give Formatted Contracts
     * @test
     */
    public function it_should_give_formatted_contracts()
    {
        //Given
        //Human Resource
        $hr = $this->factory->of(HumanResource::class)->create();
        //Physical Resource
        $pr = $this->factory->of(PhysicalResource::class)->times(2)->create();
        //Contract
        $cr = BasicPurchaseContract::createContract($pr,$hr,Carbon::now())->withQuantities([4,5]);
        //When
        //Get Contracts
        $contracts = $hr->contracts($cr)->with('resources')->get();
        $uniq = uniqueArray($contracts->toArray());
        eval(\Psy\sh());
        //Then
        //Assert Type Of Returned Contracts
        $this->assertEquals(get_class($cr), $contracts[0]["type"]);
    }
}
