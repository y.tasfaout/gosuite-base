<?php

namespace Tests\Unit\Base\Resources\Human;

use Orchestra\Testbench\TestCase;
use Tests\TestsSetup;
use Tests\MigrationSetup;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Gosuite\Base\Resources\Human\HumanResource;
use Gosuite\Base\Resources\Human\HumanResourceRole;
use Gosuite\Base\Resources\Physical\PhysicalResource;
use Gosuite\Base\Resources\Resource;
use Gosuite\Base\Core\Action;
class HumanResourceRoleTest extends TestCase
{
    use RefreshDatabase;
    use TestsSetup;
    use MigrationSetup;

    protected function setUp()
    {
        parent::setUp();
        $this->migrate();
        $this->factory = $this->loadFactories();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * Create roles for human resource
     * @test
     */
    public function it_should_create_role()
    {
        //Given a role
        $role = new HumanResourceRole();
        //When
        $role->name = "Administrator";
        $role->description = "This is an administrator";
        $role->type = 0;
        $role->save();
        //Then
        $this->assertCount(1,HumanResourceRole::get());
    }

    /**
     * Assign role to a human resource
     * @test
     */
    public function it_should_assign_role()
    {
        //Given
        $hr = $this->factory->of(HumanResource::class)->create();
        $role = new HumanResourceRole(["name" => "admin", "description" => "Application admin","type" => 0]);
        $role->save();
        //When
        $hr->assignRole($role);
        //Then
        $this->assertEquals(0, $hr->roles()->first()->type);
    }

    /**
     * Detach role to a human resource
     * @test
     */
    public function it_should_detach_role()
    {
        //Given
        $hr = $this->factory->of(HumanResource::class)->create();
        $role = new HumanResourceRole(["name" => "admin", "description" => "Application admin","type" => 0]);
        $role->save();
        //When
        $hr->assignRole($role);
        $hr->removeRole();
        //Then
        $this->assertCount(0, $hr->roles()->get());
    }


    /**
     * It Should Update Role
     * @test
     */
    public function it_should_update_role()
    {
        //Given
        $hr = $this->factory->of(HumanResource::class)->create();
        $role1 = new HumanResourceRole(["name" => "admin", "description" => "Application admin","type" => 0]);
        $role1->save();
        $role2 = new HumanResourceRole(["name" => "agent", "description" => "Yet another role","type" => 1]);
        $role2->save();
        //When
        $hr->assignRole($role1);
        $hr->updateRole($role2);
        //Then
        $this->assertEquals($role2->name, $hr->roles()->first()->name);
        $this->assertCount(1,$hr->roles()->get());
    }
    /**
     * List role resources
     * @test
     */
    public function it_should_list_role_actions()
    {
        //Given
        $role = new HumanResourceRole(["name" => "admin", "description" => "Application admin","type" => 0]);
        $action = new Action(["resource_type" => HumanResource::class, "action_type" => "c"]);
        $action->save();
        $role->save();
        //When
        $role->actions()->save($action,["resource_type"=> $action->resource_type]);
        //Then
        $this->assertCount(1, $role->actions()->get());
    }

    /**
     * It should tell you if the current resource can perform a given action on a resource
     * @test
     */
    public function it_should_check_permission_of_human_resource_on_a_resource()
    {
        //Given

        $hr = $this->factory->of(HumanResource::class)->create();

        $role = new HumanResourceRole(["name" => "agent", "description" => "Communication agent", "type" => 3]);
        $role->save();

        $actionsArray = array('c','r','u','d');
        foreach ($actionsArray as $actionType) {
            $action = new Action(["resource_type" => PhysicalResource::class, "action_type" => $actionType]); // Supported Actions
            $action->save();
        }

        $allowedAction = new Action(["resource_type" => PhysicalResource::class, "action_type" => 'r']);

        $role->actions()->save($allowedAction,["resource_type"=> $action->resource_type]);
        $hr->assignRole($role);

        //When
        $pr = $this->factory->of(PhysicalResource::class)->create();

        $permissions = $pr->can($hr)->do(['d','r','c']);
//        eval(\Psy\sh());
        //Then
        $this->assertFalse($permissions['d']);

    }

    
}
