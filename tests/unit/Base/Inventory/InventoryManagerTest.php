<?php

namespace Tests\Unit\Base\Inventory;

use Tests\TestsSetup;
use Tests\MigrationSetup;
use Faker\Generator as Faker;
use Orchestra\Testbench\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use \Gosuite\Base\Inventory\InventoryManager;
use Carbon\Carbon;
use \Gosuite\Base\Resources\Contracts\IPhysicalResource;
use \Gosuite\Base\Resources\Physical\PhysicalResource;
use \Gosuite\Base\Resources\Human\HumanResource;
use Illuminate\Database\Eloquent\Factory;

class InventoryManagerTest extends TestCase
{
    use RefreshDatabase;
    use TestsSetup;
    use MigrationSetup;
    protected $basicContract;
    protected $tenant;
    protected $rentable;

    public function setUp()
    {
        parent::setUp();
        $this->migrate();
        $this->factory = $this->loadFactories();
    }

    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * It should add quantity to a resource
     * @test
     */
    public function it_should_add_qtn_to_resource()
    {
        //Given
        $pr = $this->factory->of(PhysicalResource::class)->create();
        $qtn = 2;
        $unit = 'Bottles';
        //When
        $invManager = InventoryManager::createQuantity($pr,$qtn, $unit);
        //Then
        $this->assertEquals(2, $invManager->qtn);
    }
}