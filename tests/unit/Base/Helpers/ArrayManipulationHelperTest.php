<?php

namespace Tests\Unit\Helpers;

use Orchestra\Testbench\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestsSetup;
use Tests\MigrationSetup;
class ArrayManipulationHelperTest extends TestCase
{
  use TestsSetup;
  use MigrationSetup;
  /**
   * Setup
   */
  public function setUp(){
    parent::setUp();
  }
  /**
   * Tear Down
   */
  public function tearDown(){
    parent::tearDown();
  }
  /**
   * It Should Check In Collection Of Objects If They Have A JSON Property
   * Then Convert It To An Array And Return The New Collection
   * @test
   */
  public function it_should_convert_any_json_value_to_an_array()
  {
    //Given
    $testCase = [
      [
        "label" => "value1",
        "json" => json_encode(["name" => "first json"])
      ],
      [
        "label" => "value2",
        "json" => json_encode(["name" => "second json"])
      ]
    ];
    //When
    $result = EmbededJsonToArray($testCase);
    //Then
    $this->assertEquals('first json', $result[0]['json']["name"]);
    $this->assertEquals('second json', $result[1]['json']["name"]);
  }
}
