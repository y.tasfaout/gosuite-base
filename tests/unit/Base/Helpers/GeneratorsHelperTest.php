<?php

namespace Tests\Unit\Helpers;

use Orchestra\Testbench\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Carbon\Carbon;
use Tests\TestsSetup;
use Tests\MigrationSetup;
class GeneratorsHelperTest extends TestCase
{
	use TestsSetup;
	use MigrationSetup;
	/**
	 * Setup
	 */
	public function setUp(){
		parent::setUp();
	}
	/**
	 * Tear Down
	 */
	public function tearDown(){
		parent::tearDown();
	}
   /**
    * Generate a unique identifier for contract
    * @template 19021200001
    * @test
    */
   public function it_should_generate_unique_identifier()
   {
   		//Given a date and name
   		$date = Carbon::now();
   		$lastContractId = 12;
   		//When generating contract code
   		$code = rentContractCode($date, $lastContractId);
   		//Then
        $this->assertStringEndsWith('00013' , $code);
   }


    /**
     * Get Last Id
     * @test
     */
    public function it_should_get_last_id(){
        $testCase = "22041900013";
        $lastId = getLastContractId($testCase);
        $this->assertEquals(13, $lastId);
    }
   /**
    * Should never generate the same thing when a rent contract is generated twice in the same day
    * @test
    */
   public function it_should_always_stay_unique_even_in_same_day()
   {
   		//Given a date and name
   		$date = Carbon::now();
   		$name = 'yassine';
   		//When generating contract code first time
   		$code1 = rentContractCode($date, $name);
   		//When generating another contract
   		$code2 = rentContractCode($date, $name);
   		//Then
   		$this->assertTrue($code1 != $code2);
   }
   
   /**
    * Should generate binding contract code
    *
    * @test
    */
    public function it_should_generate_binding_contract_code()
    {
        //Given a date
        $date = new Carbon();
        $leftCount = 1;
        $rightCount = 10;
        $contractId = 22;
        //When
        $code = bindingContractCode($date, $leftCount, $rightCount); // 1-22-10/111218
        //Then
        $this->assertRegExp("/^[0-9-]*\/[0-9]*$/", $code, 'It should match the regex');
    }
}
