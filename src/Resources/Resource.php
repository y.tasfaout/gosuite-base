<?php
/**
 * Created by PhpStorm.
 * User: yassine
 * Date: 11/19/18
 * Time: 9:38 PM
 */

namespace Gosuite\Base\Resources;


use Illuminate\Database\Eloquent\Model;
use Gosuite\Base\Resources\Human\HumanResource;
use Gosuite\Base\Resources\Physical\PhysicalResource;
class Resource extends Model
{
    protected $fillable = ['resource_id', 'resource_type'];
    public $timestamps = false;
    /**
     * Register HumanResource or PhysicalResource
     * @param mixed $resource
     */
    public static function register($resource)
    {
        if(!self::where('resource_id', $resource->id)->exists()){
            $_this = new self();
            $_this->resource_id = $resource->id;
            $_this->resource_type = ($resource->type == HumanResource::class ||  $resource->type == PhysicalResource::class) ? $resource->type : get_parent_class($resource);
            $_this->save();
            return $_this;
        }
        else{
            return self::where('resource_id', $resource->id)->first();
        }
    }
    /**
     * @return Namepsace type of resource
     */
    public function type()
    {
        return $this->resource_type;
    }


    /**
     * Returns Original Resource Model
     * @return mixed resource
     */
    public function toOrigin()
    {
        switch ($this->resource_type) {
            case HumanResource::class:
                $hr = $this->human()->first();
                return $hr;
                break;
            case PhysicalResource::class:
                $pr = $this->physical()->first();
                return $pr;
                break;
            default:
                return $this;
                break;
        }
    }
    /**
     * Link to HumanResource
     */
    public function human()
    {
        if($this->resource_type == HumanResource::class)
        {
            return $this->belongsTo('Gosuite\Base\Resources\Human\HumanResource','resource_id', 'id');
        }
    }
    /**
     * Link to PhysicalResource
     */
    public function physical()
    {
        if($this->resource_type == PhysicalResource::class)
        {
        return $this->belongsTo('Gosuite\Base\Resources\Physical\PhysicalResource','resource_id', 'id');
        }
    }

    /**
     * Link to binding contracts
     */
    public function contracts($direction)
    {
        if($direction == 'left'){
            return $this->belongsToMany('Gosuite\Base\Acquisition\BasicBindingContract','bindings','left_resource_id', 'contract_id');
        }else{
            return $this->belongsToMany('Gosuite\Base\Acquisition\BasicBindingContract', 'bindings','right_resource_id', 'contract_id');
        }
    }
}