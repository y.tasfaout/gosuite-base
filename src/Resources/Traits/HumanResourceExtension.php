<?php
namespace Gosuite\Base\Resources\Traits;

trait HumanResourceExtension
{
  protected $HRModule; // Human Resource Module
  public $resource; // Resource Object
  public function create($data)
  {
    $this->resource = new $this->HRModule($data);
    $this->resource->type = get_class($this);
    return $this->resource;
  }
   //Get All Or Specific Instances of that type
  public function get($resourceId = null)
  {
    if (is_null($resourceId)) return $this->HRModule::get()->where('type', get_class($this));
    return $this->HRModule::where('type', get_class($this))->where('id', $resourceId)->first();
  }

   //Get Model
  public function find($resourceId)
  {
    $this->resource = $this->HRModule::findOrFail($resourceId);
    return $this;
  }
  public function __get($name)
  {
    $definition = isset($this->resource->definition) ? json_decode($this->resource->definition, true) : null;
    if (isset($definition[$name])) return $definition[$name];
    return $this->resource->{$name};
  }
}

