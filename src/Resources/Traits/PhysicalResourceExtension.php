<?php
namespace Gosuite\Base\Resources\Traits;

use Gosuite\Base\Resources\Physical\Contracts\IPhysicalResource;


trait PhysicalResourceExtension
{
  // protected $PRModule; // Physical Resource Module
  // public $resource; // Resource Object
  // public function create($data)
  // {
  //   $this->resource = new $this->PRModule($data);
  //   $this->resource->type = get_class($this);
  //   return $this->resource;
  // }

  // //Get All Or Specific Instances of that type In A Collection
  // public function get($resourceId = null)
  // {
  //   if (is_null($resourceId)) return $this->PRModule::get()->where('type', get_class($this));
  //   return $this->PRModule::where('type', get_class($this))->where('id', $resourceId)->first();
  // }

  // //Get Model
  // public function find($resourceId)
  // {
  //   $this->resource = $this->PRModule::findOrFail($resourceId);
  //   return $this;
  // }
  // //Update Resource
  // public function update($resourceId, array $updatedResource)
  // {
  //   $this->PRModule::find($resourceId)->update($updatedResource);
  //   return $this->PRModule::find($resourceId)->first();
  // }

  // //Delete Resource
  // public function remove($resourceId)
  // {
  //   $this->resource = $this->PRModule::find($resourceId)->first();
  //   $this->resource->remove();
  // }


  // //Manipulate Field Getter
  // public function __get($name)
  // {
  //   $definition = isset($this->resource->definition) ? json_decode($this->resource->definition, true) : null;
  //   if (isset($definition[$name])) return $definition[$name];
  //   return $this->resource->{$name};
  // }

  //Traverse Tree
  public function traverse()
  {
    return $this->resource::descendantsOf($this->resource->id)->toTree($this->resource->id);
  }
  // //Flat Tree
  // public function flatTraverse()
  // {
  //   return $this->resource->where('type', 'App\Access\Buildings\Door')->get()->toFlatTree();
  // }

  // //Allow The Class To Morph From PhysicalResource To The Class Itself
  // public function getClassFromResource(IPhysicalResource $resource)
  // {
  //   $this->resource = $resource;
  //   return $this;
  // }
}

