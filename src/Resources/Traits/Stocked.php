<?php

namespace Gosuite\Base\Resources\Traits;

use Gosuite\Base\Resources\Physical\PhysicalResource;
use Gosuite\Base\Acquisition\BasicRentContract;
use Gosuite\Base\Resources\Human\HumanResource;
use Carbon\Carbon;
use Gosuite\Base\Acquisition\Contracts\PurchaseStrategy;
use Gosuite\Base\Resources\Human\Contracts\IHumanResource;
use Gosuite\Base\Inventory\InventoryManager;

trait Stocked
{
    protected $_stock;
   /**
    * Set Quantity
    * @param Double $qtn
    * @param String $unit
    * @TODO Find a way to hanlde multiple inventories
    */
    public function createQuantity($qtn, $unit)
    {
        // @FIXME this is a just a HACK !!!!!!
        if (!$this->inventories()->exists()) {
            $invManager = InventoryManager::createQuantity($this, $qtn, $unit);
            return $invManager;
        } else {
            throw new \Exception('Only Supports Single Inventory ');
        }
    }

    /**
     * Get Quantity
     * @return Double $qtn
     */
    public function getQuantity(InventoryManager $inventory = null)
    {
        if ($inventory) {
            $qtn = $inventory->current_qtn;
        } else {
            $qtn = $this->inventories()->sum('current_qtn');
        }
        return $qtn;
    }

    /**
     * Update Quantity
     * @param InventoryManager $inventory
     * @param Double $qtn
     * @return InventoryManager $newInventory
     * @TODO Make sure updating both quantities won't cause issues later
     */
    public function updateQuantity(InventoryManager $inventory,$qtn)
    {
        $inventory->update(["initial_qtn" => $qtn, "current_qtn" => $qtn]);
        return $inventory;
    }

    /**
     * Consume Quantity
     * @param InventoryManager $inventory
     * @param Double $qtn
     * @return InventoryManager $newInventory
     */
    public function consumeQuantity(InventoryManager $inventory, $qtn)
    {
        $currentQtn = $inventory->current_qtn;
        $leftQtn = $currentQtn - $qtn;
        if ($leftQtn !== 0) {
            $inventory->update(["current_qtn" => $leftQtn]);
            return $inventory;
        }
    }
    /**
     * Get Unit 
     * @return String $unit
     * @todo Find better way to manage units
     */
    public function getUnit()
    {
        $unit = $this->inventories()->latest()->first()->unit;
        return $unit;
    }
    /**
     * Relation With Inventory Manager
     */
    public function inventories()
    {
        return $this->morphToMany('Gosuite\Base\Inventory\InventoryManager', 'inventoriable', 'inventoriables', 'resource_id', 'inventory_id');
    }
}

