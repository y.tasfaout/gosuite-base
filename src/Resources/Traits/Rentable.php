<?php

namespace Gosuite\Base\Resources\Traits;

use Gosuite\Base\Resources\Physical\PhysicalResource;
use Gosuite\Base\Acquisition\BasicRentContract;
use Gosuite\Base\Resources\Human\HumanResource;
use Carbon\Carbon;

trait Rentable
{
  /**
   * Rent a rousource
   * @param  HumanResource $tenant
   * @return BasicRentContract $contract
   */
  public function rent(HumanResource $tenant, Carbon $startDate, Carbon $endDate)
  {
		//Check rentable Available
    if ($this->isAvailable($startDate, $endDate)) {
			//Create a Contract
      $contractCode = rentContractCode($startDate, $tenant->first_name);
      $contract = BasicRentContract::create(["code" => $contractCode, "start_date" => $startDate, "end_date" => $endDate]);
			// Add This resource and tenant
      $contract->resources()->save($this, ["tenant_id" => $tenant->id]);
			// Return Contract
      return $contract;
    } else {
      throw new \Exception('RentableNotAvailableException');
    }
  }

  /**
   * Add A Tenant to that resource
   * @param  HumanResource $tenant
   * @return void
   */
  public function addTenant($tenant)
  {
    $contracts = $this->contracts();
    if ($contracts->exists()) {
      $contracts->each(function ($contract) use ($tenant) {
        $this->tenants()->save($tenant, ['contract_id' => $contract->id, 'rentable_id' => $this->id, 'rentable_type' => get_class($this)]);
      });
    }
  }

  /**
   * Get Tenant
   */
  public function getTenants()
  {
    return $this->tenants()->get();
  }

  /**
   * Check if a resource is available
   * @param  Carbon  $startDate
   * @param  Corbon  $endDate
   * @return boolean
   */
  public function isAvailable($startDate, $endDate)
  {
    if (!$this->contracts()->exists()) {
      return true;
    } else {
      return !$this->contracts()
        ->whereRaw('? between start_date and end_date', [$startDate])
        ->orWhereRaw('? between start_date and end_date', [$endDate])
        ->orWhereRaw('? < start_date and ? > end_date', [$startDate, $endDate])
        ->exists();
    }
  }

  /**
   * Cancel a given contract
   *
   * @param BasicRentContract $contractCode
   * @return void
   */
  public function cancelContract(BasicRentContract $contract)
  {
    $contract->cancel();
  }

  /**
   * Cancel all contracts for the selected resource
   *
   * @return void
   */
  public function cancelAllContracts()
  {
    $this->contracts()->delete();
  }

  /**
   * Remove a Rentable
   */
  public function remove()
  {
    if ($this->contracts()->exists() || $this->children()->exists()) {
      throw new \Exception("ContractsFoundOnDeleteRentable");
    } else {
      $this->delete();
    }
  }
}
