<?php

namespace Gosuite\Base\Resources\Traits;

use Gosuite\Base\Resources\Physical\PhysicalResource;
use Gosuite\Base\Acquisition\BasicRentContract;
use Gosuite\Base\Resources\Human\HumanResource;
use Carbon\Carbon;
use Gosuite\Base\Acquisition\Contracts\PurchaseStrategy;
use Gosuite\Base\Resources\Human\Contracts\IHumanResource;

trait Sellable
{
    
    /**
     * Sell a product
     * @param IHumanResource $client
     * @param Carbon $purchaseDate
     * @param Int $qtn
     * @return BasicPurchaseContract $contract
     */
    public function sell(PurchaseStrategy $strategy, IHumanResource $client, $date = Carbon::now)
    {
        $contract = $strategy->sell($this, $client, $date);
        return $contract;
    }
}
