<?php

namespace Gosuite\Base\Resources\Human;

use Illuminate\Database\Eloquent\Model;

class HumanResourceRole extends Model
{
    protected $fillable = ["type","name", "description"];
    protected $table = "roles";



    /**
     * Relationship with actions
     */
    public function actions()
    {
        return $this->belongsToMany('Gosuite\Base\Core\Action','roles_actions', 'role_id', 'action_id');
    }
}
