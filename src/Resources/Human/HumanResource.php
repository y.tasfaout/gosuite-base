<?php

namespace Gosuite\Base\Resources\Human;

use Gosuite\Base\Acquisition\BasicBindingContract;
use Gosuite\Base\Acquisition\BasicPurchaseContract;
use Gosuite\Base\Acquisition\BasicRentContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Gosuite\Base\Resources\Human\Contracts\IHumanResource;
use Gosuite\Base\Resources\Human\HumanResourceRole;
use Illuminate\Foundation\Auth\User as Authenticatable;
/**
 * Basic Rent Contract Model
 */

class HumanResource extends Authenticatable implements IHumanResource
{
  //Use Soft Delete
  use SoftDeletes;

  protected $fillable = [
    'title',
    'first_name',
    'last_name',
    'middle_name',
    'date_of_birth',
    'place_of_birth',
    'address',
    'country',
    'email',
    'phone_number',
      'is_user',
    'password',
    'type',
    'definition'
  ];
  protected $hidden = ['password', 'type', 'remember_token'];
  /**
   * Remove a human resource record
   */
  public function remove()
  {
    if ($this->contracts()->exists()) {
      throw new \Exception("ContractsFound");
    } else {
      $this->delete();
    }
  }

  /**
   * Assign role of Human Resource
   */
  public function assignRole(HumanResourceRole $role)
  {
      $this->roles()->attach($role);
  }

  /**
  * Update role of Human Resource
  */
  public function updateRole(HumanResourceRole $role)
  {
      $this->roles()->sync($role);
  }
  /**
   * Remove role 
   */
  public function removeRole()
  {
    $this->roles()->detach();
  }
  /**
   * Role Relation
   */
  public function roles()
  {
    return $this->belongsToMany('Gosuite\Base\Resources\Human\HumanResourceRole', 'human_resources_roles','role_id','human_resource_id');
  }
  /**
   * Contracts Relation
   * @param Model $type of contract
   */
  public function contracts(Model $type)
  {
      $type = get_class($type);
      $parentType = get_parent_class($type);
      if($type == get_class(new BasicRentContract) || $parentType == get_class(new BasicRentContract)) {
          return $this->belongsToMany('Gosuite\Base\Acquisition\BasicRentContract', 'rentables', 'tenant_id', 'contract_id');
      }
      else if($type == get_class(new BasicPurchaseContract) || $parentType == get_class(new BasicPurchaseContract))
      {
          return $this->belongsToMany('Gosuite\Base\Acquisition\BasicPurchaseContract', 'sellables', 'buyer_id', 'contract_id');
      }
      else
      {
          throw new \Exception('No Contracts Found');
      }
  }


}
