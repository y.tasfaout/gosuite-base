<?php

/**
 * @Author: Yassine TASFAOUT
 * @Date:   2018-04-29 15:11:47
 * @Last Modified by:   Yassine TASFAOUT
 * @Last Modified time: 2018-04-29 22:34:31
 */
namespace Gosuite\Base\Resources\Physical;

use Kalnoy\Nestedset\NodeTrait;
use Gosuite\Base\Resources\Traits\Rentable;
use Illuminate\Database\Eloquent\Model;
use Gosuite\Base\Acquisition\BasicRentContract;
use Gosuite\Base\Resources\Human\HumanResource;
use Illuminate\Database\Eloquent\SoftDeletes;
use Gosuite\Base\Resources\Physical\Contracts\IPhysicalResource;
use Gosuite\Base\Resources\Traits\Sellable;
use Gosuite\Base\Resources\Traits\Stocked;
/**
 * Basic Rent Contract Model
 */

class PhysicalResource extends Model implements IPhysicalResource
{
  use NodeTrait;
  use Rentable;
  use Sellable;
  use Stocked;
  //Use Soft Delete
  use SoftDeletes;

  protected $fillable = ["name", "description", "structural", "definition", "type"];

  protected $humanResourceActions;

    /**
     * Permission Check
     * @param
     */
    public function can(HumanResource $resource)
    {
        $this->humanResourceActions = $resource->roles()->first()->actions();
        return $this;
    }

    /**
     * Check actions
     * @param String | String[] $actionType
     */
    public function do($actionTypes)
    {
        $permissions = [];
        if(is_array($actionTypes))
        {
            foreach ($actionTypes as $actionType)
            {
                foreach ($this->humanResourceActions->get() as $action)
                {
                    if($action->action_type == $actionType)
                    {
                        $permissions[$actionType] = true;
                    }else{
                        $permissions[$actionType] = false;
                    }
                }
            }
        }else{
            foreach ($this->humanResourceActions->get() as $action)
            {
                if($action->action_type == $actionTypes)
                {
                    $permissions[$actionTypes] = true;
                }else{
                    $permissions[$actionTypes] = false;
                }
            }
        }
        return $permissions;
    }


  /**
   * Contracts Relationship
   */
  public function contracts(Model $type)
  {
      $type = get_class($type);
      $parentType = get_parent_class($type);
      if($type == get_class(new BasicRentContract) || $parentType == get_class(new BasicRentContract)) {
          return $this->morphToMany('Gosuite\Base\Acquisition\BasicRentContract', 'rentable', 'rentables', 'rentable_id', 'contract_id');
      }
      else if($type == get_class(new BasicPurchaseContract) || $parentType == get_class(new BasicPurchaseContract))
      {
          return $this->morphToMany('Gosuite\Base\Acquisition\BasicPurchaseContract', 'sellable', 'sellables', 'sellable_id', 'contract_id')->withPivot('qtn');
      }
      else
      {
          throw new \Exception('No Contracts Found');
      }
  }

  /**
   * Tenants Relation
   */
  public function tenants()
  {
    return $this->belongsToMany('Gosuite\Base\Resources\Human\HumanResource', 'rentables', 'rentable_id', 'tenant_id');
  }

}
