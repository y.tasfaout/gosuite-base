<?php

/**
 * @Author: Yassine TASFAOUT
 * @Date:   2018-04-29 10:24:43
 * @Last Modified by:   Yassine TASFAOUT
 * @Last Modified time: 2018-04-29 11:29:35
 */

function getLastContractId($code){
    if($code){
        // To Support Old Code System
        if(preg_match("/\d{4}[A-Z]{3}[A-z0-9]{4}/", $code)) {
            return 0;
        }
        return (int) substr($code, 6, strlen($code) - 5);
    }
    return 0;
}

function getLastContractYear($code){
    if($code){
        return (int) substr($code, 0, 2); // 19 20 21
    }
}

function rentContractCode($startDate, $lastContract){
    $lastContractCodeId = $lastContract ? getLastContractId($lastContract->code) :  getLastContractId(null);
    $lastContractYear = $lastContract ? getLastContractYear($lastContract->code) :  null;
    $date = $startDate->format('ymd');
    if($lastContractYear){
        if($lastContractYear == (int) $startDate->format('y')){
            return $date . str_pad($lastContractCodeId + 1, 5, '0', STR_PAD_LEFT);
        }else{
            return $date . str_pad( 1, 5, '0', STR_PAD_LEFT);
        }
    }
    return $date . str_pad( 1, 5, '0', STR_PAD_LEFT);
}

function bindingContractCode($date, $leftCount, $rightCount)
{
    $dt = $date->format('dmy');
    $code = $leftCount. '-' . $rightCount . '/' . $dt;
    return $code;
}
