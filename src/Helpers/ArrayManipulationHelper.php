<?php

function isJson($string)
{
  json_decode($string);
  return (json_last_error() == JSON_ERROR_NONE);
}

function EmbededJsonToArray($collection)
{
  $result = [];
  foreach ($collection as $item) {
    foreach ($item as $key => $value) {
      if (isJson($value)) {
        $item[$key] = json_decode($value, true);

      }
    }
    array_push($result, $item);
  }
  return $result;
}

function MakeConsecutive($array)
{
  $consecutiveArr = [];
  for ($i = $array[0]; $i <= $array[sizeof($array) - 1]; $i++) {
    $consecutiveArr[] = $i;
  }
  return $consecutiveArr;
}

function padTime($time)
{
  return strlen($time) == 1 ? "0" . $time : $time;
}


function PluckRandomValue($array)
{
  return array_rand($array, 1);
}

function uniqueArray($array)
{
    return array_map("unserialize", array_unique(array_map("serialize", $array)));
}

function isMultiDimensional(array $array)
{
    return count($array) !== count($array, COUNT_RECURSIVE);
}
