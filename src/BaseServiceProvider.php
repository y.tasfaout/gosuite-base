<?php

namespace Gosuite\Base;
use Illuminate\Support\ServiceProvider;

class BaseServiceProvider extends ServiceProvider
{
    public function boot()
    {
    
    }
    public function register()
    {
        $this->registerPublishables();
    }

    private function registerPublishables()
    {
        $basePath = dirname(__DIR__);
        $publishablesArr = [
            "migrations" => [
                "$basePath/database/migrations" => database_path("migrations")
            ],
            "factories" => [
                "$basePath/database/factories" => database_path("factories")
            ],
            "helpers" => [
                "$basePath/src/Helpers" => app_path("Helpers")
            ]
        ];
        foreach ($publishablesArr as $group => $paths) {
            $this->publishes($paths, $group);
        }
    }
}