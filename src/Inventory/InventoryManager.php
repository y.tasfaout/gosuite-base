<?php

/**
 * @Author: Yassine TASFAOUT
 * @Date:   2018-04-18 15:15:36
 * @Last Modified by:   Yassine TASFAOUT
 * @Last Modified time: 2018-04-30 10:18:39
 */
namespace Gosuite\Base\Inventory;

use Illuminate\Database\Eloquent\Model;
use Gosuite\Base\Resources\Physical\PhysicalResource;
use Gosuite\Base\Resources\Human\HumanResource;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use Gosuite\Base\Inventory\Contracts\InventoryManagerInterface;
use Gosuite\Base\Resources\Physical\Contracts\IPhysicalResource;
/**
 * Basic Rent Contract Model
 */
class InventoryManager extends Model implements InventoryManagerInterface
{
    use SoftDeletes;
    protected $fillable = ['initial_qtn', 'current_qtn', 'unit'];
    protected $table = "inventory";
    
    /**
     * Create a quantity of a physical resource
     * @param IPhysicalResource $pr
     * @param Double $qtn
     * @param String $unit
     * @return void 
     */
    public static function createQuantity(IPhysicalResource $pr, $qtn, $unit){
        $_this = new self(["initial_qtn" => $qtn, "current_qtn" => $qtn, "unit" => $unit]);
        $_this->save();
        $_this->resources()->save($pr);
        return $_this;
    }

    /**
     * A Inventory Has Many Resources & A Resource Could Be Linked To Many Inventories
     */
    public function resources()
    {
        return $this->morphedByMany('Gosuite\Base\Resources\Physical\PhysicalResource', 'inventoriable', 'inventoriables', 'inventory_id', 'resource_id');
    }
}