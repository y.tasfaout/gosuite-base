<?php

namespace Gosuite\Base\Core;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    protected $fillable = ["resource_type", "action_type"];
    protected $table = "actions";


    /**
     * @param String $resource_type Namespace strinf
     * @return Actions[] $actions Actions related to that resource
     */
    public static function byResource($resource_type)
    {
        $actions = self::where('resource_type', $resource_type);
        return $actions;
    }

}
