<?php
/**
 * Created by PhpStorm.
 * User: yassine
 * Date: 11/19/18
 * Time: 11:16 AM
 */

namespace Gosuite\Base\Acquisition\Contracts;


use Gosuite\Base\Acquisition\BasicBindingContract;

interface IBindingContract
{
    /**
     * @param mixed $left Contract Responsible (one or many)
     * @param mixed $right Contract Members (one or many)
     * @return BasicBindingContract $contract
     */
    public static function bindResources($left, $right);
}