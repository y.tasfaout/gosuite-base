<?php

/**
 * @Author: Yassine TASFAOUT
 * @Date:   2018-04-29 09:41:42
 * @Last Modified by:   Yassine TASFAOUT
 * @Last Modified time: 2018-04-29 09:55:23
 */

namespace Gosuite\Base\Acquisition\Contracts;

use Carbon\Carbon;

interface IRentContract
{
  public function cancel();
  public function extend(Carbon $newDate);
  public function revise(
    $newPhysicalResources = null,
    $newHumanResource = null,
    Carbon $newStartDate = null,
    Carbon $newEndDate = null
  );
}
