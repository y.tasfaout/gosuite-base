<?php

namespace Gosuite\Base\Acquisition\Contracts;

use Carbon\Carbon;
use Gosuite\Base\Resources\Physical\Contracts\IPhysicalResource;
use Gosuite\Base\Resources\Human\Contracts\IHumanResource;

interface PurchaseStrategy
{
    /**
     * @param IPhyscialResource or array of $physicalResources Resource to sell
     * @param IHumanResource $humanResource Resource to Sell to
     * @param Carbon $date Date of sale
     * @param Array $qtns Array of Quantities
     * @return BasicPurchaseContract 
     */
    public function sell($physicalResources, IHumanResource $humanResource, $date = Carbon::now, array $qtns);
}
