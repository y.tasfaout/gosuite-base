<?php

/**
 * @Author: Yassine TASFAOUT
 * @Date:   2018-04-18 15:15:36
 * @Last Modified by:   Yassine TASFAOUT
 * @Last Modified time: 2018-04-30 10:18:39
 */

namespace Gosuite\Base\Acquisition;

use Illuminate\Database\Eloquent\Model;
use Gosuite\Base\Resources\Human\HumanResource;
use Gosuite\Base\Resources\Human\Contracts\IHumanResource;
use Gosuite\Base\Inventory\InventoryManager;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

use Gosuite\Base\Acquisition\Contracts\IPurchaseContract;

/**
 * Basic Rent Contract Model
 */
class BasicPurchaseContract extends Model implements IPurchaseContract
{
    use SoftDeletes;
    use Contractable;
    protected $fillable = ['code', 'start_date', 'end_date','status', 'type'];
    protected $table = "contracts";
    public $contractedResources;


    /**
     * Create Contract With Quantities
     */
    public function withQuantities(array $qtns)
    {
        $index = 0;
        foreach ($this->resources()->get() as $resource) {
            $this->resources()->updateExistingPivot($resource, ["qtn" => $qtns[$index]], false);
            $index++;
        }
        return $this;
    }

    /**
     * Update Status
     * @param Integer $status
     * @return BasicPurchaseContract $this
     */
    public function setStatus($status)
    {
        $this->update(["status" => $status]);
        return $this;
    }
    /**
     * Validate Contract
     * Consume Quantities
     * @param InventoryManager $inventory
     * @return BasicPurchaseContract
     */
    public function validate(InventoryManager $inventory)
    {
        foreach ($this->resources()->get() as $resource) {
            $resource->consumeQuantity($inventory, $resource->pivot->qtn);
        }
        return $this;
    }


    /**
     * Add a Buyer to a Contract
     * @param IHumanResource $buyer
     */
    public function addBuyer(IHumanResource $buyer)
    {
        if ($this->hasResources()) {
            $this->resources()->update(["buyer_id" => $buyer->id]);
        } else {
            $this->tenants()->save($buyer);
        }
    }

    /**
     * Get All Buyers
     * @return HumanResource[]
     */
    public function getBuyer()
    {
        return $this->tenants()->get();
    }


    /**
     * Generate Contracts Directly Given
     * @param IPysicalResource $pr
     * @param IHumanResource $hr
     * @param Carbon $s_date @required
     * @param Carbon $e_date @optional
     * @FIXME Only Usable With PurchaseContract
     * @return BasicPurchaseContract
     */
    public static function createContract($pr, $hr, $s_date, $e_date = null)
    {
        $lastContract = self::orderBy('id', 'desc')->first();
        $_this = new self(["code" => rentContractCode($s_date, $lastContract), "start_date" => $s_date, "end_date" => $e_date, "type" => get_called_class()]);
        $_this->save();
        $_this->addResources($pr);
        $_this->addBuyer($hr);
        return $_this;
    }

}