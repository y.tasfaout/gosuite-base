<?php


namespace Gosuite\Base\Acquisition;

use Illuminate\Database\Eloquent\Model;
use Gosuite\Base\Resources\Human\HumanResource;
use Gosuite\Base\Resources\Human\Contracts\IHumanResource;
use Gosuite\Base\Inventory\InventoryManager;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class ContractFile extends Model{
    protected $table= "contract_files";
    protected $fillable = ["name","type", "url"];
}
