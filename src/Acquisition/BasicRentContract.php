<?php

/**
 * @Author: Yassine TASFAOUT
 * @Date:   2018-04-18 15:15:36
 * @Last Modified by:   Yassine TASFAOUT
 * @Last Modified time: 2018-04-30 10:18:39
 */

namespace Gosuite\Base\Acquisition;

use Illuminate\Database\Eloquent\Model;
use Gosuite\Base\Acquisition\Contracts\IRentContract;
use Gosuite\Base\Resources\Physical\PhysicalResource;
use Gosuite\Base\Resources\Human\HumanResource;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

/**
 * Basic Rent Contract Model
 */
class BasicRentContract extends Model implements IRentContract
{
    use SoftDeletes;
    use Contractable;
    protected $fillable = ['code', 'start_date', 'end_date', 'tenant_id'];

    protected $table = "contracts";
    public $contractedResources;

    /**
     * Revoke a rent contract
     * @return void
     */
    public function cancel()
    {
        $this->delete();
    }

    /**
     * Check if a contract is cancelled
     * @return boolean
     */
    public function isCancelled()
    {
        return $this->deleted_at !== null;
    }


    /**
     * Check if contract is expired
     */
    public function isExpired($expiryTime = null)
    {
        $today = Carbon::now();
        if (!is_null($expiryTime)) {
            $this->end_date->setTimeFromTimeString($expiryTime);
        }
        return $this->end_date <= $today;
    }


    /**
     * Check if contract has started
     */
    public function hasStarted($startingTime = null)
    {
        $today = Carbon::now();
        if (!is_null($startingTime)) {
            $this->start_date->setTimeFromTimeString($startingTime);
        }
        return $this->start_date <= $today;
    }


    public function extend(Carbon $newDate)
    {
        // End date should be offset by some time @FIXME make it as a dynamic param
        if ($this->attachedResources()->areAvailable($this->end_date->addHours(1), $newDate)) {
            $this->end_date = $newDate;
            $this->save();
        } else {
            throw new \Exception("Resources Are Not Available");
        }
    }

    public function revise(
        $newPhysicalResources = null,
        $newHumanResource = null,
        Carbon $newStartDate = null,
        Carbon $newEndDate = null
    )
    {

    }

    /**
     * Get Contracted Resources Same As $this->resources()
     */
    public function attachedResources()
    {
        $this->contractedResources = $this->resources()->get();
        return $this;
    }

    /**
     * Check Availability of All Resources
     */
    public function areAvailable(Carbon $startDate, Carbon $endDate)
    {
        $availability = [];

        foreach ($this->contractedResources as $resource) {
            array_push($availability, $resource->isAvailable($startDate, $endDate));
        }
        return array_sum($availability) == count($availability);
    }


}
