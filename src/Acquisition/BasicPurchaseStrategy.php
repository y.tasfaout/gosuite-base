<?php

/**
 * @Author: Yassine TASFAOUT
 * @Date:   2018-04-18 15:15:36
 * @Last Modified by:   Yassine TASFAOUT
 * @Last Modified time: 2018-04-30 10:18:39
 */
namespace Gosuite\Base\Acquisition;

use Illuminate\Database\Eloquent\Model;
use Gosuite\Base\Resources\Physical\PhysicalResource;
use Gosuite\Base\Resources\Human\HumanResource;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use Gosuite\Base\Acquisition\Contracts\PurchaseStrategy;
use Gosuite\Base\Resources\Physical\Contracts\IPhysicalResource;
use Gosuite\Base\Resources\Human\Contracts\IHumanResource;
/**
 * Basic Purchase Strategy 
 */
class BasicPurchaseStrategy implements PurchaseStrategy
{
    public function sell($physicalResources, IHumanResource $humanResource, $date = Carbon::now, array $qtns)
   {
        $contractCode = rentContractCode($date, $humanResource->name);
       $contract = BasicPurchaseContract::createContract($physicalResources, $humanResource, $date)->withQuantities($qtns);
        return $contract;
   }

}