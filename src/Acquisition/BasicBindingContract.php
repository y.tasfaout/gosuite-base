<?php
/**
 * Created by PhpStorm.
 * User: yassine
 * Date: 11/19/18
 * Time: 11:18 AM
 */

namespace Gosuite\Base\Acquisition;


use Gosuite\Base\Acquisition\Contracts\IBindingContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use Gosuite\Base\Acquisition\Contractable;
use Gosuite\Base\Resources\Resource;
use Gosuite\Base\Resources\Human\HumanResource;
use Illuminate\Support\Collection;
class BasicBindingContract extends Model implements IBindingContract
{
    use SoftDeletes;
    use Contractable;
    protected $fillable = ['code', 'start_date', 'end_date','status', 'type'];
    protected $table = "contracts";
    public $rightResourcesCount;
    public static function bindResources($left, $right, $s_date = null, $e_date = null)
    {
        $_this = new self(["code" => bindingContractCode(new Carbon(), $left->id, $right->id), "start_date" => $s_date, "end_date" => $e_date, "type" => get_called_class()]);
        $_this->save();
        $_this->bind($right)->to($left);
        return $_this;
    }


    /**
     * @param $resources Represent right resources
     * @return $this
     */
    public function bind($resources)
    {
        $this->rightResourcesCount = $resources->count() ?  $resources->count() : 1;
        $this->attachResources($resources, "right");
        return $this;
    }

    /**
     * @param $resources Represent left resources
     */
    public function to($resources)
    {
        $this->attachResources($resources, "left");
    }

    /**
     * Converting Resources To Generic Resources
     * @param Resource|Collection $resources
     */
    protected function toGenericResource($resources)
    {
        if($resources instanceof Collection)
        {
            $genericResources = [];
            foreach ($resources as $resource) {
               $genericResources[] = Resource::register($resource);
            }
        }else{
            $genericResources = Resource::register($resources);
        }
        return $genericResources;
    }

    /**
     * Attach resources to the left and right of the contract
     * @param $resources
     * @param $position
     */
    protected function attachResources($resources, $position)
    {
        $resources = $this->toGenericResource($resources);
        if($position == "right")
        {
            if($resources instanceof Resource)
            {
                $this->rightResources()->save($resources,["right_resource_type" => $resources->type()]);
            }else{
                foreach ($resources as $resource) {
                    $this->rightResources()->save($resource, ["right_resource_type" => $resource->type()]);
                }
            }
        }else {
            if($this->rightResources()->exists())
            {   
                if($resources instanceof Resource)
                    {
                        $this->rightResources()->update(["left_resource_id" => $resources->id, "left_resource_type" => $resources->type()]);
                    }
                else
                    { 
                        foreach ($this->rightResources()->get() as $rightResource)
                        {
                             $this->rightResources()->detach($rightResource->id);
                            foreach ($resources as $resource) {
                                $this->rightResources()->attach($rightResource->id,["left_resource_id" => $resource->id , "left_resource_type" => $resource->type(), "right_resource_type" => $rightResource->type()]);
                            }
                        }
                    }
            }
        }   
    }

    /**
     * Updates Contract Resources
     * @param $direction
     * @param $resources
     */
    public function updateResources($direction, $resources)
    {
        //Getting Generic Resources
        $genericResources = $this->toGenericResource($resources);
            if(!is_object($genericResources)){
                $resourceIds = array_map(function($ele) { return $ele['id'];}, $genericResources);
            }else{
                $resourceIds = [$genericResources['id']];
            }
        if($direction == 'left')
        {
            $index = 0;
            foreach($this->rightResources()->get() as $rightResource) {
                foreach ($resourceIds as $id)
                {
                    if($index < 1) {
                        $this->leftResources()->sync([$id => ['right_resource_id' => $rightResource->id, 'right_resource_type' => $rightResource->type()]]);
                    }else{
                        $this->leftResources()->syncWithoutDetaching([$id => ['right_resource_id' => $rightResource->id, 'right_resource_type' => $rightResource->type()]]);
                    }
                    $index++;
                }
            }
        }
        else{
                $index = 0;
            foreach($this->leftResources()->get() as $leftResource) {
                foreach ($resourceIds as $id) {
                    if($index < 1) {
                        $this->rightResources()->sync([$id => ['left_resource_id' => $leftResource->id, 'left_resource_type' => $leftResource->type()]]);
                    }
                    else{
                        $this->rightResources()->syncWithoutDetaching([$id => ['left_resource_id' => $leftResource->id, 'left_resource_type' => $leftResource->type()]]);
                    }
                    $index++;
                }
            }
        }
    }

    public function getLeftResources()
    {
        return $this->leftResources()->get()->unique();
    }
    public function getRightResources()
    {
        return $this->rightResources()->get()->unique();
    }
    public function leftResources()
    {
        return $this->belongsToMany('Gosuite\Base\Resources\Resource','bindings','contract_id', 'left_resource_id')->withTimestamps();
    }

    public function rightResources()
    {
        return $this->belongsToMany('Gosuite\Base\Resources\Resource','bindings', 'contract_id', 'right_resource_id')->withTimestamps();
    }
}