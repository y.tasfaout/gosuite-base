<?php

namespace Gosuite\Base\Acquisition;

use Gosuite\Base\Inventory\InventoryManager;
use Gosuite\Base\Resources\Physical\PhysicalResource;
use Gosuite\Base\Acquisition\BasicRentContract;
use Gosuite\Base\Resources\Human\HumanResource;
use Carbon\Carbon;
use Gosuite\Base\Resources\Physical\Contracts\IPhysicalResource;
use Gosuite\Base\Resources\Human\Contracts\IHumanResource;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
trait Contractable
{
    /**
     * Add Resources to contract
     * @param PhysicalResource[] $pResources
     */
    public function addResources($pResources)
    {
        if ($pResources instanceof PhysicalResource) {
            if ($pResources->children()->exists()) {
                $this->addNestedResources($pResources);
            }// Has Children
            else {
                if (!$this->resources()->find($pResources->id)) {
                    $this->saveResource($pResources);
                }
            }// Does not have children
        }//instance of physical resource
        else {
            foreach ($pResources as $pResource) {
                if (!$pResource->structural) {
                    $this->saveResource($pResource);
                }
            }
        }//collection of physical resources
        return $this;
    }


    /**
     * Add Nested Physical Resources
     * @param PhysicalResource $pResources Tree of PhysicalResources
     */
    protected function addNestedResources($pResources)
    {
        $this->saveResource($pResources);
        foreach ($pResources->children()->get() as $child) {
            $this->addNestedResources($child);
        }
    }

    /**
     * Save Resources That Are Not Structural
     * @param PhysicalResource $pResources
     * @return void
     */
    protected function saveResource($pResource)
    {
        if (!$pResource->structural) {
            $this->resources()->save($pResource);
        }
    }

    /**
     * Add a Tenant to a Rentable to this Contract
     * @param IHumanResource $tenants
     * @param IPhysicalResource $rentable
     * @FIXME Refactor to a repository
     */
    public function addTenant(IHumanResource $tenants, IPhysicalResource $rentable)
    {
        $this->tenants()->save($tenants, ["rentable_id" => $rentable->id, "rentable_type" => get_class($rentable)]);
    }


    /**
     * Get All Tenants
     * @return HumanResource[]
     */
    public function getTenants()
    {
        return $this->tenants()->get();
    }


    /**
     * Check If Contract Has Resources
     * @return  Boolean
     */
    protected function hasResources()
    {
        return $this->resources()->exists();
    }

    /**
     * Add File To Contract
     */
    public function addFile($file, $type){
        try{
            $path = Storage::disk('contracts')->putFile('contracts',$file);
            $newFile = new ContractFile(["name"=> $file->name, "type" =>$type, "url"=> $path]);
            $savedFile = $this->files()->save($newFile);
            return $savedFile;
        }
        catch(\Exception $ex)
        {
            return false;
        }
    }
    /**
     * A Contract Has Many Resources & A Resource Could Be Linked To Many Contracts
     */
    public function resources()
    {
        if(get_class($this) == BasicPurchaseContract::class || get_parent_class($this) == BasicPurchaseContract::class) {
            return $this->morphedByMany('Gosuite\Base\Resources\Physical\PhysicalResource', 'sellable', 'sellables', 'contract_id', 'sellable_id')->withPivot('qtn','buyer_id');
        } else {
            return $this->morphedByMany('Gosuite\Base\Resources\Physical\PhysicalResource', 'rentable', 'rentables', 'contract_id', 'rentable_id');
        }
    }

    /**
     * Tenants Relation
     */
    public function tenants()
    {

        if(get_class($this) == BasicPurchaseContract::class || get_parent_class($this) == BasicPurchaseContract::class) {
            return $this->belongsToMany('Gosuite\Base\Resources\Human\HumanResource', 'sellables', 'contract_id', 'buyer_id');
        } else {
            return $this->belongsToMany('Gosuite\Base\Resources\Human\HumanResource', 'rentables', 'contract_id', 'tenant_id');
        }
    }

    /**
     * Files Relation
     */
    public function files()
    {
        return $this->hasMany('Gosuite\Base\Acquisition\ContractFile','contract_id','id');
    }

}