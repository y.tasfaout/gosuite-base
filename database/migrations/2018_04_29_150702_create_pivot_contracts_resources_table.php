<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreatePivotContractsResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('rentables')){
            Schema::create('rentables', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('tenant_id')->unsigned()->nullable();
                $table->integer('contract_id')->unsigned()->index();
                $table->integer('rentable_id')->unsigned()->nullable();
                $table->string('rentable_type')->nullable();
            });
        }
        DB::statement('ALTER TABLE rentables ADD CONSTRAINT uniq_tenant_resource UNIQUE (contract_id,tenant_id);');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rentables');
    }
}
