<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoriableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('inventoriables')) {
            Schema::create('inventoriables', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('resource_id')->unsigned()->nullable();
                $table->integer('inventory_id')->unsigned()->index();
                $table->string('inventoriable_type')->nullable();
            });
            DB::statement('ALTER TABLE inventoriables ADD CONSTRAINT uniq_resource_inventory UNIQUE (inventory_id,resource_id);');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventoriable');
    }
}
