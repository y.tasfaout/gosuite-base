<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHumanResourcesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('human_resources', function (Blueprint $table) {
      $table->increments('id');
      $table->string('title')->nullable();
      $table->string('first_name');
      $table->string('last_name');
      $table->string('middle_name')->nullable();
      $table->date('date_of_birth')->nullable();
      $table->string('place_of_birth')->nullable();
      $table->text('address')->nullable();
      $table->string('country')->nullable();
      $table->string('email')->unique()->nullable();
      $table->string('phone_number')->nullable();
      $table->string('password')->nullable();
      $table->boolean('is_user')->default(0);
      $table->json('definition')->nullable();
      $table->string('type')->default('App\\\Base\\\Resources\\\Human\\\HumanResource');
      $table->rememberToken();
      $table->softDeletes();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('rent_contracts');
    Schema::dropIfExists('human_resources');
  }
}
