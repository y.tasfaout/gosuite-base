<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('sellables')) {
            Schema::create('sellables', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('buyer_id')->unsigned()->nullable();
                $table->integer('sellable_id')->unsigned()->nullable();
                $table->integer('contract_id')->unsigned()->index();
                $table->double('qtn', 8, 2)->nullable()->default(0);
                $table->string('sellable_type')->nullable();
            });
        }
        DB::statement('ALTER TABLE sellables ADD CONSTRAINT uniq_sellable_contract UNIQUE (contract_id,sellable_id);');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sellables');
    }
}
