<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBindingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bindings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('left_resource_id')->unsigned()->nullable();
            $table->integer('right_resource_id')->unsigned()->nullable();
            $table->string('left_resource_type')->nullable();
            $table->string('right_resource_type')->nullable();
            $table->integer('contract_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bindings');
    }
}
