<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Kalnoy\Nestedset\NestedSet;
class CreatePhysicalResourcesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
      if (!Schema::hasTable('physical_resource')) {
          Schema::create('physical_resources', function (Blueprint $table) {
              $table->increments('id');
              $table->string('name');
              $table->text('description');
              $table->json('definition')->nullable(); //This will have other dynamic props of the resource
              $table->string('type')->default("App\\\Base\\\Resources\\\Physical\\\PhysicalResource");
              $table->boolean('structural')->default(0);
              NestedSet::columns($table);
              $table->softDeletes();
              $table->timestamps();
          });

      }
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('physical_resources');
  }
}
