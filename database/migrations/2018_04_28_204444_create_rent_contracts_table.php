<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRentContractsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    if (!Schema::hasTable('contracts')) {
      Schema::create('contracts', function (Blueprint $table) {
        $table->increments('id');
        $table->string('code');
        $table->dateTime('start_date')->nullable();
        $table->dateTime('end_date')->nullable();
          $table->integer('status')->unsigned()->nullable();
        $table->string('type')->default("App\\\Base\\\Acquisition\\\BasicRentContract");
        $table->softDeletes();
        $table->timestamps();
      });
    }
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('contracts');
  }
}
