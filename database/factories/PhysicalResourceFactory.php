<?php

use Faker\Generator as Faker;
use Gosuite\Base\Resources\Physical\PhysicalResource;
use Illuminate\Database\Eloquent\Factory;
$factory->define(PhysicalResource::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->text,
        'type' => PhysicalResource::class
    ];
});
