<?php

use Faker\Generator as Faker;
use Gosuite\Base\Resources\Human\HumanResource;
use Illuminate\Support\Facades\Hash;

$factory->define(HumanResource::class, function (Faker $faker) {
    return [
        'title'         => $faker->title,
        'first_name'    => $faker->firstName,
        'last_name'     => $faker->lastName,
        'middle_name'   => $faker->randomLetter,
        'date_of_birth' => $faker->date,
        'place_of_birth'=> $faker->city,
        'address'       => $faker->address,
        'country'       => $faker->country,
        'email'         => $faker->safeEmail,
        'phone_number'  => $faker->phoneNumber,
        'password'      =>  Hash::make('secret'),
        'type'          => HumanResource::class
    ];
});
