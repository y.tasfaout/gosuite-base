<?php

use Faker\Generator as Faker;
use Gosuite\Base\Resources\Human\HumanResource;
use Gosuite\Base\Acquisition\BasicPurchaseContract;

$factory->define(BasicPurchaseContract::class, function (Faker $faker) {
    return [
        'code' => rentContractCode($faker->dateTimeBetween($startDate = 'now', $endDate = '+ 1 day', $timezone = null), $faker->name),
        'start_date' => $faker->dateTimeBetween($startDate = 'now', $endDate = '+ 1 days', $timezone = null),
    ];
});
