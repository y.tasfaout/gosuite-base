<?php

use Faker\Generator as Faker;
use Gosuite\Base\Acquisition\BasicRentContract;
use Gosuite\Base\Resources\Human\HumanResource;
$factory->define(BasicRentContract::class, function (Faker $faker) {
    return [
    	'code' => rentContractCode($faker->dateTimeBetween($startDate = 'now', $endDate = '+ 5 days', $timezone = null), $faker->name),
        'start_date' => $faker->dateTimeBetween($startDate = 'now', $endDate = '+ 5 days', $timezone = null),
        'end_date' => $faker->dateTimeBetween($startDate = 'now', $endDate = '+ 5 days', $timezone = null)
	];
});
